#include <Eigen/Dense>
using namespace Eigen;

#include <algorithm>
#include <cmath>
#include <cstdio>
#include <fstream>
#include <iomanip>
#include <string>
#include <vector>
#include <array>
#include <random>

#include "conf.hpp"
#include "disk.hpp"
#include "globals.hpp"
#include "grid.hpp"
#include "misc.hpp"
#include "pn.hpp"
#include "propagator.hpp"
#include "system.hpp"
#include "units.hpp"

const unsigned int DEFAULT_SEED = 1337U;

// Helper function for z-sorting, when lambdas cannot be used
// (GCC <= 4.4)
#ifdef __GNUC__
static bool z_sort(const Vector3d &a, const Vector3d &b) {
    return a.z() < b.z();
};
#endif

void State::init_binary_wo_model() {
    t = 0;

    // Init binary to 1990 coordinates
    bin.r << -3.112472944622833e+001, -2.076049122885863e+001, 1.821658702517051e+004;
    bin.v << -1.512354933361768e+002, -5.768750519149843e+002, 2.122761241275696e+002;
    bin.s << -9.676477244886807e-002, 2.925783479988131e-002, -9.948771571986633e-001;
    bin.chi = m.spin_chi;

    // Calculate coordinates in CM frame
    update_rcm();

    // Update constants of motion and store initial values
    update_constants();

    bin.E_kep0 = bin.E_kep;
    bin.E_gr0 = bin.E_gr;
    bin.h0 = bin.h;
    bin.E_gr = 0;

    // Kludge time limits
    g_progconf.start_year = 1988.219177836114300;
}

void State::init_binary() {
    t = 0;
    double g = C_G;

    // Calculate apocenter position and velocity
    double apodist = (1 + m.ecc) * m.a;
    double apovel = sqrt(g * (m.m1 + m.m2) / m.a * (1 - m.ecc) / (1 + m.ecc));
    bin.r << 0, sin(m.apo_theta), cos(m.apo_theta);
    bin.r *= apodist;
    if (g_progconf.tweak_enabled("apocentre_velocity")) {
        // XXX: Conforming to the sign error in initial values
        // in Valtonen/Mikkola code
        bin.v << 0, -cos(m.apo_theta),
            sin(m.apo_theta) * (m.m1 - m.m2) / (m.m1 + m.m2);
    } else {
        // Correct version
        bin.v << 0, -cos(m.apo_theta), sin(m.apo_theta);
    }

    bin.v *= apovel;

    // Calculate coordinates in CM frame
    update_rcm();

    // Calculate spin from spherical coordinates
    bin.s << cos(m.spin_phi) * sin(m.spin_theta),
        sin(m.spin_phi) * sin(m.spin_theta), cos(m.spin_theta);
    bin.chi = m.spin_chi;

    if (g_progconf.debug_enabled("initial_state")) {
        g_debug_stream << "apodist = " << stringify(apodist) << "\n";
        g_debug_stream << "apovel  = " << stringify(apovel) << "\n";
    }

    // Update constants of motion and store initial values
    update_constants();

    bin.E_kep0 = bin.E_kep;
    bin.E_gr0 = bin.E_gr;
    bin.h0 = bin.h;
    bin.E_gr = 0;
}

// Helper
static void kepler_disk(disk &d, disk::ParticleInfo info, int n, double rin,
                        double rout, double z0, int sense, double mass,
                        Vector3d &roff, Vector3d &voff) {

    static int idcnt = 0;
    double g = C_G;

    d.init(n);

    // Create a thin disk with a keplerian velocity distribution and
    // uniform distribution in phi and r. => 1/r density distribution
    // The disk will rotate in the same sense as the black hole, so
    // we will check the direction of the spin.
    std::mt19937_64 gen;
    gen.seed(DEFAULT_SEED);

    std::uniform_real_distribution<double> rdist(rin, rout);
    std::uniform_real_distribution<double> phidist(0, 2 * M_PI);
    std::uniform_real_distribution<double> zdist(0, 1);

    // need probability \propto r, so weights = intervals
    std::array<double,2> intervals = {rin, rout};
    std::piecewise_linear_distribution<double> rdist2(
            intervals.begin(), intervals.end(), intervals.begin());

    double r, phi, vel, z;
    Vector3d rv, vv;

    // Add a spiral perturbation to the disk
    double pert_m = 2;
    double pert_k = 2 * M_PI / (rout - rin); // 0.001;
    double pert_eps_r = 0.1;
    double pert_eps_phi = 0.1;
    double pert_r0 = rout - rin;

    for (int i = 0; i < n; i++) {
        // Set id, normal state, and which disk we belong to
        d.ids.push_back(idcnt++);
        d.states.push_back(disk::NORMAL);
        d.infos.push_back(info);

        // For r^-1 surface density
        //r = rdist(gen);
        // For constant surface density
        r = rdist2(gen);
        phi = phidist(gen);

        // Get z-coordinate from sech^2 distribution, which we get
        // by transforming z ~ U(0,1) by z0 * atanh(2z - 1)
        z = zdist(gen);
        z = z0 * std::atanh(2 * z - 1);

        // Keplerian velocity, in azimuthal direction
        vel = sqrt(g * mass / sqrt(r * r + z * z));

        rv << cos(phi) * r, sin(phi) * r, z;
        vv << -sense * sin(phi) * vel, sense * cos(phi) * vel, 0.0;

        // the spiral perturbation
        if (0) {
            // velocity perturbation components
            double r_pert = pert_eps_r * exp(-(r - rin) / pert_r0) *
                            cos(pert_m * phi - pert_k * (r - rin));
            double phi_pert = pert_eps_phi * exp(-(r - rin) / pert_r0) *
                              cos(pert_m * phi - pert_k * (r - rin));
            // velocity perturbation vector
            Vector3d v_azi_pert, v_r_pert;
            v_azi_pert << -sense * sin(phi) * vel * phi_pert,
                sense * cos(phi) * vel * phi_pert, 0.0;
            v_r_pert << cos(phi) * vel * r_pert, sin(phi) * vel * r_pert, 0.0;
            vv += v_azi_pert + v_r_pert;
        }

        // Translate to move around primary, also
        // give the same initial velocity as the primary has
        rv += roff;
        vv += voff;

        d.rs.push_back(rv);
        d.vs.push_back(vv);
    }
}

void State::init_disk() {
    int sense = (m.spin_theta > M_PI / 2 ? -1 : 1);
    kepler_disk(disk1, disk::DISK_M1, m.disk1.n_parts, m.disk1.r_inner,
                m.disk1.r_outer, m.disk1.scale_height, sense, m.m1, bin.rcm1,
                bin.vcm1);
    kepler_disk(disk2, disk::DISK_M2, m.disk2.n_parts, m.disk2.r_inner,
                m.disk2.r_outer, m.disk2.scale_height, sense, m.m2, bin.rcm2,
                bin.vcm2);
}

void State::reconstruct(const Model &m_) {
    // Copy the model
    m = m_;

    // Precalculate constants
    m_tot = m.m1 + m.m2;
    m_rel1 = m.m1 / m_tot;
    m_rel2 = m.m2 / m_tot;
    r_acc1 = 2 * C_G * m.m1 / (C_c * C_c) * m.r_acc1;
    r_acc2 = 2 * C_G * m.m2 / (C_c * C_c) * m.r_acc2;

    // Precalculate PN values
    pn_do_precalcs(pc_bin, m.m1, m.m2, m.spin_chi, m.q);
    pn_do_precalcs(pc_disk_m1, m.m1, 0, m.spin_chi, m.q);
    pn_do_precalcs(pc_disk_m2, m.m2, 0, 0, 0);

    // Initialize binary
    // TODO: This must be manually toggled ATM
    init_binary();
    // init_binary_wo_model();

    // Initialize disk
    init_disk();

    // Zero stat counters
    stats.accretion_total = stats.escape_total = 0;
    clear_stat_accumulators();

    // Initialize gridwise stats
    // TODO: Currently for m1 disk only
    stats.cell.reserve(m.disk1.n_grid_r);
    for (int i = 0; i < m.disk1.n_grid_r; i++) {
        stats.cell.push_back(std::vector<gridcell_stats>(m.disk1.n_grid_phi));
        for (int j = 0; j < m.disk1.n_grid_phi; j++) {
            gridcell_stats s = {0, 0, 0};
            stats.cell[i].push_back(s);
        }
    }

    if (g_progconf.debug_enabled("initial_state")) {
        g_debug_stream << "Precalcs:"
                       << " m_tot = " << m_tot << " m_rel1 = " << m_rel1
                       << " m_rel2 = " << m_rel2 << " r_acc1 = " << r_acc1
                       << " r_acc2 = " << r_acc2 << "\n";
    }
}

std::string State::bin_to_string() const {
    std::stringstream ss;
    ss << std::fixed << std::setprecision(15) << physical_time() << " "
       << std::scientific
       // Relative
       << bin.r.transpose().format(eigen_format) << " "
       << bin.v.transpose().format(eigen_format) << " "
       // CM
       << bin.rcm1.transpose().format(eigen_format) << " "
       << bin.vcm1.transpose().format(eigen_format) << " "
       << bin.rcm2.transpose().format(eigen_format) << " "
       << bin.vcm2.transpose().format(eigen_format) << " "
       // Spin
       << bin.s.transpose().format(eigen_format) << " "
       // Constants of motion
       << (bin.E_kep - bin.E_kep0 - bin.E_gr) / bin.E_kep0 << " "
       << bin.E_gr / bin.E_kep0 << " " << bin.h.transpose().format(eigen_format)
       << " ";
    return ss.str();
}

// Print helper
static void print_disk(const State &s, const disk &d, std::stringstream &ss) {
    ss << std::scientific;
    for (int i = 0; i < d.n; i++) {
        if (d.states[i] != disk::NORMAL) continue;
        ss << std::setprecision(15) << s.physical_time() << " " << d.ids[i]
           << " "
           << " " << d.infos[i] << " " << d.states[i] << " "
           << d.rs[i].transpose().format(eigen_format) << " "
           << d.vs[i].transpose().format(eigen_format) << "\n";
    }
}

std::string State::disk_to_string() const {
    std::stringstream ss;
    print_disk(*this, disk1, ss);
    ss << "\n\n";
    print_disk(*this, disk2, ss);
    return ss.str();
}

std::string State::stat_accumulators_to_string() const {
    std::stringstream ss;
    ss << std::fixed << std::setprecision(15) << physical_time();
    for (int i = 0; i < 4; i++)
        ss << " " << stats.accretion_count[i];
    ss << " " << stats.inclination_escape_count;
    return ss.str();
}

double State::physical_time() const {
    double scaling = (g_progconf.unit_G ? 1 / sqrt(C_IAU_G) : 1);
    // TODO: Valtonen/Mikkola has a strange factor 1/0.2054 as a
    // scaling of time. Where does it come from?
    if (g_progconf.tweak_enabled("time"))
        return sqrt(C_IAU_G) * scaling * t * 0.2054 + g_progconf.start_year;

    return scaling * (1 + m.z) * t + g_progconf.start_year;
}

void State::update_constants() {
// XXX: Scaled by 1/total mass
#if 1
    bin.E_kep = 0.5 * m_rel1 * m_rel2 * bin.v.dot(bin.v) -
                C_G * (m.m1 * m.m2 / m_tot) / bin.r.norm();
    bin.h = m_rel1 * m_rel2 * bin.r.cross(bin.v);
#endif
#if 0
	// In CM frame
	bin.E_kep = 
		0.5*m_rel1* bin.vcm1.dot(bin.vcm1)
		+ 0.5*m_rel2* bin.vcm2.dot(bin.vcm2)
		- C_G*(m.m1*m.m2/m_tot)/bin.r.norm();
	bin.h = m_rel1*bin.rcm1.cross(bin.vcm1)
		+ m_rel2*bin.rcm2.cross(bin.vcm2);
#endif
}

void State::update_grid_stats() {
    int nphi = g_grid1.get_nphi(), nr = g_grid1.get_nr();
    // Calculate:
    // - mean, median and stddev of z-component for each gridcell
    for (int i = 0; i < nr; i++) {
        for (int j = 0; j < nphi; j++) {
            double zm = 0, zmed = 0, zsdev = 0;
            std::vector<int> &is = g_grid1[i][j];
            size_t npts = is.size();
            if (npts <= 0) {
                stats.cell[i][j].z_dev = 0;
                stats.cell[i][j].z_mean = 0;
                stats.cell[i][j].z_median = 0;
                continue;
            }

            // Make a new vector for sorting purposes
            std::vector<Vector3d> pts;
            pts.reserve(npts);
            for (unsigned int k = 0; k < npts; k++) {
                pts.push_back(disk1.rs[is[k]]);
                zm += disk1.rs[is[k]][2];
            }

            // Calculate mean
            zm /= npts;

            for (unsigned int k = 0; k < npts; k++) {
                double d = disk1.rs[is[k]][2] - zm;
                zsdev += d * d;
            }
            zsdev = sqrt(zsdev / (npts - 1));

// Sort the points by z value to calculate median
#if defined(__GNUC__)
            std::sort(pts.begin(), pts.end(), z_sort);
#else
            std::sort(pts.begin(), pts.end(),
                      [](const Vector3d &a, const Vector3d &b) {
                          return a.z() < b.z();
                      });
#endif

            // Calculate the median
            {
                size_t n = pts.size();
                if (n % 2 == 0)
                    zmed = 0.5 * (pts[n / 2 - 1].z() + pts[n / 2].z());
                else
                    zmed = pts[n / 2].z();
            }

            // Store stats
            stats.cell[i][j].z_mean = zm;
            stats.cell[i][j].z_median = zmed;
            stats.cell[i][j].z_dev = zsdev;
        }
    }
}

void State::update_element_stats() {
    // Orbital elements
    const double mu = C_G * m.m1;
    double oe[6];
    double acc[6][2] = {{0}};
    double min[6] = {0};
    double max[6] = {0};
    int num = 0;

    for (int i = 0; i < 6; i++) {
        min[i] = std::numeric_limits<double>::max();
        max[i] = std::numeric_limits<double>::min();
    }

// Mean, min, max
#pragma omp parallel for private(oe) if (g_progconf.use_openmp)
    for (int i = 0; i < disk1.n; i++) {
        if (disk1.states[i] != disk::NORMAL) continue;

#pragma omp atomic
        num++;

        // Calculate orbital elements
        Vector3d pos = disk1.rs[i] - bin.rcm1;
        Vector3d vel = disk1.vs[i] - bin.vcm1;
        rv2oe_gp(mu, pos, vel, oe);

// Handle angles separately. Order is:
// a (scalar)
// e (scalar)
// i (angle)
// Omega (angle)
// omega (angle)
// M (angle)
#pragma omp atomic
        acc[0][0] += oe[0];
#pragma omp atomic
        acc[1][0] += oe[1];
#pragma omp atomic
        acc[2][0] += std::cos(oe[2]);
#pragma omp atomic
        acc[2][1] += std::sin(oe[2]);
#pragma omp atomic
        acc[3][0] += std::cos(oe[3]);
#pragma omp atomic
        acc[3][1] += std::sin(oe[3]);
#pragma omp atomic
        acc[4][0] += std::cos(oe[4]);
#pragma omp atomic
        acc[4][1] += std::sin(oe[4]);
#pragma omp atomic
        acc[5][0] += std::cos(oe[5]);
#pragma omp atomic
        acc[5][1] += std::sin(oe[5]);

#pragma omp critical
        {
            for (int j = 0; j < 6; ++j) {
                {
                    min[j] = std::min(oe[j], min[j]);
                    max[j] = std::max(oe[j], max[j]);
                }
            }
        }
    }

    for (int j = 0; j <= 1; ++j) {
        // Store mean in acc[j][0], disregard acc[j][1]
        // (which doesn't even have any meaningful data)
        acc[j][0] /= num;
        stats.orbital_elements[j][1] = min[j];
        stats.orbital_elements[j][2] = max[j];
    }

    for (int j = 2; j <= 5; ++j) {
        acc[j][0] /= num;
        acc[j][1] /= num;
        stats.orbital_elements[j][1] = min[j];
        stats.orbital_elements[j][2] = max[j];

        // We can compute standard deviation for angles right away:
        // S(z) = sqrt(-2 * log |mean(z)|) = sqrt(log(1/|mean(z)|^2))
        double z2 = acc[j][0] * acc[j][0] + acc[j][1] * acc[j][1];
        stats.orbital_elements[j][3] = sqrt(log(1 / z2));
    }

    // Store means
    stats.orbital_elements[0][0] = acc[0][0];
    stats.orbital_elements[1][0] = acc[1][0];
    stats.orbital_elements[2][0] = std::atan2(acc[2][1], acc[2][0]);
    stats.orbital_elements[3][0] = std::atan2(acc[3][1], acc[3][0]);
    stats.orbital_elements[4][0] = std::atan2(acc[4][1], acc[4][0]);
    stats.orbital_elements[5][0] = std::atan2(acc[5][1], acc[5][0]);

    // Calculate standard deviations for the first two orbital elements
    acc[0][0] = acc[1][0] = 0;
#pragma omp parallel for private(oe) if (g_progconf.use_openmp)
    for (int i = 0; i < disk1.n; i++) {
        if (disk1.states[i] != disk::NORMAL) continue;
        // Calculate orbital elements
        Vector3d pos = disk1.rs[i] - bin.rcm1;
        Vector3d vel = disk1.vs[i] - bin.vcm1;
        rv2oe_gp(mu, pos, vel, oe);

        // Handle angles separately. Order is:
        // a (scalar)
        // e (scalar)
        // i (angle)
        // Omega (angle)
        // omega (angle)
        // M (angle)
        double d1 = oe[0] - stats.orbital_elements[0][0];
        double d2 = oe[1] - stats.orbital_elements[1][0];
#pragma omp atomic
        acc[0][0] += d1 * d1;
#pragma omp atomic
        acc[1][0] += d2 * d2;
    }
    stats.orbital_elements[0][3] = sqrt(acc[0][0] / (num - 1));
    stats.orbital_elements[1][3] = sqrt(acc[1][0] / (num - 1));
}

void State::update_rcm() {
    bin.rcm1 = -m_rel2 * bin.r;
    bin.vcm1 = -m_rel2 * bin.v;
    bin.rcm2 = m_rel1 * bin.r;
    bin.vcm2 = m_rel1 * bin.v;
}

// Calculate disk radial statistics
std::string State::disk_radstats_to_string() {
    std::stringstream ss;
    int nphi = g_grid1.get_nphi(), nr = g_grid1.get_nr();
    // Calculate:
    // - mean + stddev z-component for each r-annulus
    // - mean + stddev r for each annulus
    for (int i = 0; i < nr; i++) {
        double zm = 0, zsdev = 0;
        double rm = 0, rsdev = 0;
        size_t n = 0;
        for (int j = 0; j < nphi; j++) {
            std::vector<int> &is = g_grid1[i][j];
            for (unsigned int k = 0; k < is.size(); k++) {
                zm += disk1.rs[is[k]][2];
                rm += disk1.rs[is[k]].norm();
            }
            n += is.size();
        }
        zm /= n;
        rm /= n;
        for (int j = 0; j < nphi; j++) {
            std::vector<int> &is = g_grid1[i][j];
            for (unsigned int k = 0; k < is.size(); k++) {
                double d = disk1.rs[is[k]][2] - zm;
                zsdev += d * d;
                d = disk1.rs[is[k]].norm() - rm;
                rsdev += d * d;
            }
        }
        zsdev = sqrt(zsdev / (n - 1));
        rsdev = sqrt(rsdev / (n - 1));

        // output n, minr, maxr, area, <r>, stddev(r), <z>, stddev(z)
        ss << std::setprecision(15) << std::scientific << n << " "
           << g_grid1.cell_rmin(i) << " " << g_grid1.cell_rmax(i) << " "
           << g_grid1.cell_area(i) * nphi << " " << rm << " " << rsdev << " "
           << zm << " " << zsdev << "\n";
    }
    return ss.str();
}

std::string State::disk_oe_stats_to_string() const {
    std::stringstream ss;
    ss << std::setprecision(15) << std::scientific;
    ss << physical_time();
    for (int i = 0; i < 6; i++) {
        for (int j = 0; j < 4; j++)
            ss << " " << stats.orbital_elements[i][j];
    }
    return ss.str();
}
