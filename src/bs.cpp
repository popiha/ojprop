#include "bs.hpp"
#include "system.hpp"
#include "units.hpp"
#include "pn.hpp"
#include "propagator.hpp"
#include "globals.hpp"

#include <cstdio>
#include <cstdlib>
#include <iomanip>
#include <cmath>

#include <algorithm>



bool check_vec(Vector3d &v, int line, char *file) {
	for (int i=0; i < 3; i++)
		if (!std::isfinite(v[i])) {
			g_debug_stream << "time " << g_state.physical_time() 
				<< " nan at " << line << " in " << file << " vec " << v.transpose() << std::endl;
			return false;
		}

	return true;
}

extern double omega_binary(const State &s);

/* Bulirsch-Stoer propagation. Adapted from Press et al. 1992. */

/* helper functions to marshall to/from state */
// Number of state variables
// = 1 + 9 + N*6 + 2 = 12 + N*6
static inline int n_vars(State &s) { 
	if (g_progconf.fast_disk)
		return 12;

	int ret = 12 + 6*(s.disk1.n + s.disk2.n); 
	return ret; 
}

static inline int n_vars_binary(State &s) { 
	return 12;
}

void bin2y(State &s, double *res) {
	double sw = sqrt(s.ttlw);
	res[0] = 0;
	res[1] = s.t;  
	res[2] = s.bin.r[0];
	res[3] = s.bin.r[1];
	res[4] = s.bin.r[2];
	res[5] = s.bin.v[0] /sw;
	res[6] = s.bin.v[1] /sw;
	res[7] = s.bin.v[2] /sw;
	res[8] = s.bin.s[0];
	res[9] = s.bin.s[1];
	res[10] = s.bin.s[2];
	res[11] = s.ttlw;
	res[12] = s.bin.E_gr;

	int nd = s.disk1.n + s.disk2.n;
	for (int i=0; i < s.disk1.n; i++) {
		res[13+i*3] 		 = s.disk1.rs[i][0];
		res[13+i*3+1] 		 = s.disk1.rs[i][1];
		res[13+i*3+2] 		 = s.disk1.rs[i][2];
		res[13+nd*3+i*3] 	 = s.disk1.vs[i][0] /sw;
		res[13+nd*3+i*3+1] 	 = s.disk1.vs[i][1] /sw;
		res[13+nd*3+i*3+2] 	 = s.disk1.vs[i][2] /sw;
	}
	for (int i=s.disk1.n; i < s.disk1.n + s.disk2.n; i++) {
		int j = i-s.disk1.n;
		res[13+i*3] 		 = s.disk2.rs[j][0];
		res[13+i*3+1] 		 = s.disk2.rs[j][1];
		res[13+i*3+2] 		 = s.disk2.rs[j][2];
		res[13+nd*3+i*3] 	 = s.disk2.vs[j][0] /sw;
		res[13+nd*3+i*3+1] 	 = s.disk2.vs[j][1] /sw;
		res[13+nd*3+i*3+2] 	 = s.disk2.vs[j][2] /sw;
	}
}

void y2bin(double *y, State &s) {
	double sw = sqrt(y[11]);
	s.t = y[1];
	s.bin.r[0] = y[2];
	s.bin.r[1] = y[3];
	s.bin.r[2] = y[4];
	s.bin.v[0] = y[5] * sw;
	s.bin.v[1] = y[6] * sw;
	s.bin.v[2] = y[7] * sw;
	s.bin.s[0] = y[8];
	s.bin.s[1] = y[9];
	s.bin.s[2] = y[10];
	s.ttlw 	   = y[11];
	s.bin.E_gr = y[12];
	const int nd = s.disk1.n + s.disk2.n;
	for (int i=0; i < s.disk1.n; i++) {
		s.disk1.rs[i][0] = y[13+i*3]; 
		s.disk1.rs[i][1] = y[13+i*3+1]; 			
		s.disk1.rs[i][2] = y[13+i*3+2]; 			
		s.disk1.vs[i][0] = y[13+nd*3+i*3]   * sw; 		
		s.disk1.vs[i][1] = y[13+nd*3+i*3+1] * sw; 	
		s.disk1.vs[i][2] = y[13+nd*3+i*3+2] * sw; 	
	}
	for (int i=s.disk1.n; i < s.disk1.n+s.disk2.n; i++) {
		int j = i-s.disk1.n;
		s.disk2.rs[j][0] = y[13+i*3]; 
		s.disk2.rs[j][1] = y[13+i*3+1]; 			
		s.disk2.rs[j][2] = y[13+i*3+2]; 			
		s.disk2.vs[j][0] = y[13+nd*3+i*3]   * sw; 		
		s.disk2.vs[j][1] = y[13+nd*3+i*3+1] * sw; 	
		s.disk2.vs[j][2] = y[13+nd*3+i*3+2] * sw; 	
	}
}

void rleap_disk(disk &d, double hs) {
	std::vector<Vector3d> &rs = d.rs;
#pragma omp parallel for shared(rs) if (g_progconf.use_openmp)
	for (int i=0; i < d.n; i++) {
		if (d.states[i] != disk::NORMAL) continue;
		rs[i] += hs*d.vs[i];
	}
}

void rleap(State &s, double hs) {
	// DEBUG
	debug_print(stderr, "xt", s.bin.r);
	debug_print(stderr, "dt", hs);
	debug_print(stderr, "vt", s.bin.v);

	//std::cerr << "rij " << s.bin.r.transpose() << std::endl;
	//std::cerr << "vij " << s.bin.v.transpose() << std::endl;
	//std::cerr << "hs " << hs << std::endl;

	s.t += hs;
	s.bin.r += hs*s.bin.v;
	s.update_rcm();

	rleap_disk(s.disk1, hs);
	rleap_disk(s.disk2, hs);

	// Update grid positions
	g_grid1.position = s.bin.rcm1;
	g_grid2.position = s.bin.rcm2;
}

void vleap_kep_disk(State &s, disk &d, double hs, VectorXd &kepfd) {
#pragma omp parallel for shared(kepfd) if (g_progconf.use_openmp)
	for (int i=0; i < d.n; i++) {
		if (d.states[i] != disk::NORMAL) continue;
		// XXX: CM coords
		Vector3d r1 = d.rs[i] - s.bin.rcm1;
		Vector3d r2 = d.rs[i] - s.bin.rcm2;
		double d1 = r1.norm();
		double d2 = r2.norm();
		Vector3d dr1 = r1/(d1*d1*d1);
		Vector3d dr2 = r2/(d2*d2*d2);

		Vector3d acc = -C_G*( s.m.m1*dr1 + s.m.m2*dr2 );
		kepfd.segment<3>(3*i) = acc;
	}
}

void vleap(State &s, double h) {
	const double pn_eps = g_progconf.pn_epsilon;
	double omega = (g_progconf.use_TTL ? omega_binary(s) : 1);
	double hs = h/omega;

	//int nd = (g_progconf.fast_disk ? 0 : s.disk.n);
	int nd1 = (g_progconf.fast_disk ? 0 : s.disk1.n);
	int nd2 = (g_progconf.fast_disk ? 0 : s.disk2.n);

	// 
	// Keplerian force for binary and disk
	double r12 = s.bin.r.norm();
	Vector3d dr12 = s.bin.r/(r12*r12*r12);
	Vector3d kepf = -C_G*s.m_tot*dr12; // binary total force/kick

	VectorXd kepfd1(3*nd1+1); // disk total force/kick
	VectorXd viscofd1(3*nd1+1); // viscous force

	VectorXd kepfd2(3*nd2+1); // disk total force/kick
	VectorXd viscofd2(3*nd2+1); // viscous force

	// Zero all dynamically allocated vectors
	kepfd1.setZero();
	kepfd2.setZero();
	viscofd1.setZero();
	viscofd2.setZero();

	// Iterate over disk particles and collect Kepler force
	vleap_kep_disk(s, s.disk1, hs, kepfd1);
	vleap_kep_disk(s, s.disk2, hs, kepfd2);

	// Add disk internal interactions to kepler force, to average
	// them over the step
	if (!g_progconf.fast_disk) {
		disk_interaction_f(s.disk1, g_grid1, s.m.disk1, viscofd1, s.bin.rcm1, s.bin.vcm1);
		disk_interaction_f(s.disk2, g_grid2, s.m.disk2, viscofd2, s.bin.rcm2, s.bin.vcm2);

        // debug print the relative strenghts of kepler vs. viscous
        // force
        if (g_progconf.debug_enabled("viscof")) {
            for (int i=0; i < nd1; i++) {
                fprintf(stderr, "viscof, kepf, viscof/kepf: %g %g %g\n", 
					viscofd1.segment<3>(3*i).norm(), kepfd1.segment<3>(3*i).norm(), 
					viscofd1.segment<3>(3*i).norm()/kepfd1.segment<3>(3*i).norm());
            }
            for (int i=0; i < nd2; i++) {
                fprintf(stderr, "viscof, kepf, viscof/kepf: %g %g %g\n", 
					viscofd2.segment<3>(3*i).norm(), kepfd2.segment<3>(3*i).norm(), 
					viscofd2.segment<3>(3*i).norm()/kepfd2.segment<3>(3*i).norm());
            }
		}
		// debug print the max relative strength of visco vs kepler
        if (g_progconf.debug_enabled("visco_vs_kepler")) {
			int mi = 0;
			double mv = 0, mk = 0, mrat = 0;
            for (int i=0; i < nd1; i++) {
				double rat = viscofd1.segment<3>(3*i).norm()/kepfd1.segment<3>(3*i).norm();
				if (rat > mv) {
					mrat =  rat;
					mv = viscofd1.segment<3>(3*i).norm();
					mk = kepfd1.segment<3>(3*i).norm();
					mi = i;
				}
            }
			g_debug_stream << "max visco/kep 1.: pt. no, " << mi 
				<< " v, " << mv
				<< " k, " << mk
				<< " v/k, " << mrat
				<< "\n";
			mi = 0;
			mv = 0, mk = 0, mrat = 0;
            for (int i=0; i < nd2; i++) {
				double rat = viscofd2.segment<3>(3*i).norm()/kepfd1.segment<3>(3*i).norm();
				if (rat > mv) {
					mrat =  rat;
					mv = viscofd2.segment<3>(3*i).norm();
					mk = kepfd2.segment<3>(3*i).norm();
					mi = i;
				}
            }
			g_debug_stream << "max visco/kep 2.: pt. no, " << mi 
				<< " v, " << mv
				<< " k, " << mk
				<< " v/k, " << mrat
				<< "\n";
			//fprintf(stderr, "max visco/kep: pt. no, v, k, v/k : %d, %g, %g, %g\n", mi, mv, mk, mrat);
		}
		kepfd1 += viscofd1;
		kepfd2 += viscofd2;
	}

	// Iteration for velocity dependent forces: 
	// - PN-corrections
	// x disk internal interactions (averaged over step)
	Vector3d va = s.bin.v + hs/2*kepf; // binary average (kepler) velocity
	VectorXd vad1(3*nd1+1); // disk average velocities
	VectorXd vad2(3*nd2+1); // disk average velocities

	vad1 = hs/2*kepfd1;
	vad2 = hs/2*kepfd2;

#pragma omp parallel for shared(vad1) if (g_progconf.use_openmp)
	for (int i=0; i < nd1; i++) {
		if (s.disk1.states[i] != disk::NORMAL) continue;
		//vad1.segment<3>(3*i) = s.disk1.vs[i] + hs/2*kepfd1.segment<3>(3*i);
		vad1.segment<3>(3*i) += s.disk1.vs[i];
	}
#pragma omp parallel for shared(vad2) if (g_progconf.use_openmp)
	for (int i=0; i < nd2; i++) {
		if (s.disk2.states[i] != disk::NORMAL) continue;
		//vad2.segment<3>(3*i) = s.disk2.vs[i] + hs/2*kepfd2.segment<3>(3*i);
		vad2.segment<3>(3*i) += s.disk2.vs[i];
	}

	// Check errors by comparing to sum of absolute values of velocity and spin components
	double av = va.array().abs().sum();
	double dav1 = vad1.array().abs().sum();
	double dav2 = vad2.array().abs().sum();
	Vector3d sp = s.bin.s; // spin
	double asp = sp.array().abs().sum();

	Vector3d dv = Vector3d::Zero(), dsp = Vector3d::Zero();
	VectorXd dvd1(3*nd1+1); 
	VectorXd dvd2(3*nd2+1); 
	double abserr=0, dabserr1=0, dabserr2=0, spabserr=0;
	for (int n=0; n < g_progconf.pn_max_it; n++) {
		// Binary PN-correction
		dv = pn_terms(s.pc_bin, s.bin.r, va) + pn_spin_terms(s.pc_bin, s.bin.r, va, sp);
		dsp = pn_sdot(s.pc_bin, s.bin.r, va, sp);

		//std::cerr << "pnacc " << dv.transpose().format(eigen_format) << std::endl;
		//std::cerr << "pndsp " << dsp.transpose().format(eigen_format) << std::endl;
		// Disk PN-correction
		dvd1.setZero();
		dvd2.setZero();
#pragma omp parallel for shared(dvd1) if (g_progconf.use_openmp)
		for (int i=0; i < nd1; i++) {
			if (s.disk1.states[i] != disk::NORMAL) continue;
			// XXX: CM coords
			Vector3d r1 = s.disk1.rs[i] - s.bin.rcm1;
			Vector3d r2 = s.disk1.rs[i] - s.bin.rcm2;
			Vector3d v1 = vad1.segment<3>(3*i) + s.m_rel2*va;
			Vector3d v2 = vad1.segment<3>(3*i) - s.m_rel1*va;

			// TODO: For now, we handle diverging PN-correction for disk particles by setting to zero. 
			// Not elegant.
			if (g_progconf.use_disk_m1_PN) {
				Vector3d t1 = pn_terms(s.pc_disk_m1, r1, v1); // The problematic term, can overflow => NANs
				Vector3d t2 = pn_spin_terms(s.pc_disk_m1, r1, v1, sp);
				if (!check_vec(t1,__LINE__,__FILE__))
					t1.setZero();
				if (!check_vec(t2,__LINE__,__FILE__))
					t2.setZero();
				dvd1.segment<3>(3*i) += t1 + t2;
			}
			if (g_progconf.use_disk_m2_PN) {
				Vector3d t1 = pn_terms(s.pc_disk_m2, r2, v2);
				if (!check_vec(t1,__LINE__,__FILE__))
					t1.setZero();
				dvd1.segment<3>(3*i) += t1;
			}
		}

#pragma omp parallel for shared(dvd2) if (g_progconf.use_openmp)
		for (int i=0; i < nd2; i++) {
			if (s.disk2.states[i] != disk::NORMAL) continue;
			// XXX: CM coords
			Vector3d r1 = s.disk2.rs[i] - s.bin.rcm1;
			Vector3d r2 = s.disk2.rs[i] - s.bin.rcm2;
			Vector3d v1 = vad2.segment<3>(3*i) + s.m_rel2*va;
			Vector3d v2 = vad2.segment<3>(3*i) - s.m_rel1*va;

#if 1
			if (g_progconf.use_disk_m1_PN) {
				Vector3d t1 = pn_terms(s.pc_disk_m1, r1, v1);
				Vector3d t2 = pn_spin_terms(s.pc_disk_m1, r1, v1, sp);
				if (!check_vec(t1,__LINE__,__FILE__))
					t1.setZero();
				if (!check_vec(t2,__LINE__,__FILE__))
					t2.setZero();
				dvd2.segment<3>(3*i) += t1 + t2;
			}
			if (g_progconf.use_disk_m2_PN) {
				Vector3d t1 = pn_terms(s.pc_disk_m2, r2, v2);
				if (!check_vec(t1,__LINE__,__FILE__))
					t1.setZero();
				dvd2.segment<3>(3*i) += t1;
			}
#endif
		}

		// Update average velocities
		Vector3d va_old = va;
		VectorXd vad1_old = vad1;
		VectorXd vad2_old = vad2;
		Vector3d sp_old = sp;

		va = s.bin.v + hs/2*(kepf + dv);
		vad1 = hs/2*(kepfd1 + dvd1);
		vad2 = hs/2*(kepfd2 + dvd2);
#pragma omp parallel for shared(vad1) if (g_progconf.use_openmp)
		for (int i=0; i < nd1; i++)  {
			vad1.segment<3>(3*i) += s.disk1.vs[i];
		}
        // DEBUG:
		//if (!std::isfinite(vad2[3*911])) g_debug_stream  << "it " << n << " vad2[3*911] nan at " << __LINE__ << std::endl;
#pragma omp parallel for shared(vad2) if (g_progconf.use_openmp)
		for (int i=0; i < nd2; i++)  {
			vad2.segment<3>(3*i) += s.disk2.vs[i];
		}
		sp = s.bin.s + hs/2*dsp;
		

		// Check for convergence
		double old_abserr   = abserr;
		double old_dabserr1 = dabserr1;
		double old_dabserr2 = dabserr2;
		double old_spabserr = spabserr;
		abserr = (va-va_old).array().abs().sum();
		dabserr1 = (vad1-vad1_old).array().abs().sum();
		dabserr2 = (vad2-vad2_old).array().abs().sum();
		spabserr = (sp-sp_old).array().abs().sum();

        // guard against possible divergence
		if (n >= 1 && ((dabserr1 > old_dabserr1) || (dabserr2 > old_dabserr2)))
			break;

		//std::cerr << "kepf " << kepf.transpose() << std::endl;
		//std::cerr << "dv   " << dv.transpose() << std::endl;
		//std::cerr << "va   " << va.transpose() << std::endl;
		//std::cerr << "dsp  " << dsp.transpose() << std::endl;

		// Compare difference between sum of absolute values of
		// updated and previous velocity components
		//if (abserr < g_progconf.pn_epsilon*av) 
		if (abserr <= pn_eps * av && dabserr1 <= pn_eps * dav1 && dabserr2 <= pn_eps * dav2 && spabserr <= pn_eps * asp) 
			break;
	}

	// check binary and disk errors with && (and) => shouldn't decrease binary precision at least
	//if (abserr >= g_progconf.pn_epsilon*av)
	if (abserr > pn_eps*av || dabserr1 > pn_eps * dav1 || dabserr2 > pn_eps * dav2 || spabserr > pn_eps * asp) {
		fprintf(stderr, "abserr:    %g %g*av: %g\n", abserr,   pn_eps, pn_eps*av);
		fprintf(stderr, "dabserr1:  %g %g*av: %g\n", dabserr1, pn_eps, pn_eps*dav1);
		fprintf(stderr, "dabserr2:  %g %g*av: %g\n", dabserr2, pn_eps, pn_eps*dav2);
		fprintf(stderr, "spabserr:  %g %g*av: %g\n", spabserr, pn_eps, pn_eps*asp);
	}

	// Use total average force over step
	kepf += dv;
	if (!g_progconf.fast_disk) {
		kepfd1 += dvd1;
		kepfd2 += dvd2;
	}

	// Binary energies should probably be scaled down by m?
	// XXX: Scaled by 1/total mass
	double egdot = s.m_rel1*s.m_rel2*va.dot(dv);

	// Advance velocities
	s.bin.v += hs*kepf;
	// CM
	s.update_rcm();

	// Disks
	std::vector<Vector3d> &vs1 = s.disk1.vs;
#pragma omp parallel for shared(vs1) if (g_progconf.use_openmp)
	for (int i=0; i < nd1; i++) {
		//s.disk1.vs[i] += hs*kepfd1.segment<3>(3*i);
		vs1[i] += hs*kepfd1.segment<3>(3*i);
	}
	std::vector<Vector3d> &vs2 = s.disk2.vs;
#pragma omp parallel for shared(vs2) if (g_progconf.use_openmp)
	for (int i=0; i < nd2; i++) {
		//s.disk2.vs[i] += hs*kepfd2.segment<3>(3*i);
		vs2[i] += hs*kepfd2.segment<3>(3*i);
	}

	// Advance spin
	s.bin.s += hs*dsp;

	// Calculate wdot
	// XXX: Only take w from the binary, do not add disk-binary
	// distances or the binary orbit will suffer a slight error
	double wdot = -va.dot(dr12);

	// Update W and energy
	s.ttlw += hs*wdot;
	s.bin.E_gr += hs*egdot;

	//std::cerr << "rij " << s.bin.r.transpose() << std::endl;
	//std::cerr << "vij " << va.transpose() << std::endl;
	//std::cerr << "dr12 " << dr12.transpose() << std::endl;
	//std::cerr << "wdot " << wdot << " egdot " << egdot << std::endl;
	//std::cerr << "hs " << hs << std::endl;
	//std::exit(0);

	// DEBUG
	debug_print(stderr, "kepf", kepf);
	debug_print(stderr, "dsp", dsp);
	debug_print(stderr, "dsp*chi", dsp*s.bin.chi);
	//exit(0);
}


/* helper function to perform a series of lf2 steps */
void bssubsteps(const State &sin, double h, int nsteps, double out[]) {
	/* size of substep */
	h /= nsteps;
	// TODO: Duplicating the state takes a lot of memory for many
	// particles. Do not duplicate the whole state?
	/* copy state */
	State s = sin;
	//memcpy(&c, &s, sizeof(state));

	double hs = (g_progconf.use_TTL ? h/s.ttlw : h);
	// DEBUG:
	debug_print(stderr, "nsteps", nsteps);
	debug_print(stderr, "h", h);
	debug_print(stderr, "hs", hs);
	debug_print(stderr, "W", s.ttlw);
	debug_print(stderr, "dt", hs/2);

	// r-leap
	rleap(s, hs/2);

	// Alternate v and r leaps
	for (int n = 0; n < nsteps; n++) {
		// v-leap
		vleap(s, h);

		// Check for final half-step
		hs = (g_progconf.use_TTL ? h/s.ttlw : h);
		if (n == nsteps-1) hs /= 2;

		// r-leap
		rleap(s, hs);
	}
	
	// marshall state into output
	bin2y(s, out);
}

/* various NR macros re-implemented */
#define SQR(x) ((x)*(x))
static inline double FMAX(double a, double b) { return (a > b ? a : b); }
static inline double FMIN(double a, double b) { return (a < b ? a : b); }

/* NR memory allocation */
static double* vector(int start, int num) {
	double *a;
	if ((a = (double*)malloc((num+start)*sizeof(double))) == NULL) {
		fprintf(stderr, "malloc: failed\n");
		exit(EXIT_FAILURE);
	}
	return a;
}

static double** matrix(int sr, int nr, int sc, int nc) {
	double **a;
	int i;
	if ((a = (double**)malloc((nr+sr)*sizeof(double*))) == NULL) {
		fprintf(stderr, "malloc: failed\n");
		exit(EXIT_FAILURE);
	}
	for (i = sr; i <= nr; i++) {
		if ((a[i] = (double*)malloc((nc+sc)*sizeof(double))) == NULL) {
			fprintf(stderr, "malloc: failed\n");
			exit(EXIT_FAILURE);
		}
	}
	return a;
}

static void free_vector(double *v, int start, int num) {
	free(v);
}

static void free_matrix(double **m, int sr, int nr, int sc, int nc) {
	int i;
	for (i = sr; i <= nr; i++)
		free(m[i]);
	free(m);
}

#define KMAXX 8 /* Maximum row number used in the extrapolation. orig: 8 */
#define IMAXX (KMAXX+1) 
#define SAFE1 0.25 /* Safety factors. */
#define SAFE2 0.7
#define REDMAX 1.0e-5 /* Maximum factor for stepsize reduction. */
#define REDMIN 0.7 /* Minimum factor for stepsize reduction. */
#define TINY 1.0e-30 /* Prevents division by zero. */
/* 1/SCALMX is the maximum factor by which a stepsize can be increased. */
#define SCALMX 0.1 

/* Use polynomial extrapolation to evaluate nv functions at x = 0 by fitting a
 * polynomial to a sequence of estimates with progressively smaller values x =
 * xest, and corresponding function vectors yest[1..nv]. This call is number
 * iest in the sequence of calls. Extrapolated function values are output as
 * yz[1..nv], and their estimated error is output as dy[1..nv].
 */

/* Pointers to matrix and vector used by pzextr or rzextr. */
double **d,*x;
static void pzextr(int iest, double xest, double yest[], double yz[], 
		double dy[], int nv) {
	int k1,j;
	double q,f2,f1,delta,*c;
	c=vector(1,nv);
	x[iest]=xest; /* Save current independent variable. */
	for (j=1;j<=nv;j++) dy[j]=yz[j]=yest[j];
	/* Store first estimate in first column. */
	if (iest == 1) { 
		for (j=1;j<=nv;j++) d[j][1]=yest[j];
	} 
	else {
		for (j=1;j<=nv;j++) c[j]=yest[j];
		for (k1=1;k1<iest;k1++) {
			delta=1.0/(x[iest-k1]-xest);
			f1=xest*delta;
			f2=x[iest-k1]*delta;
			for (j=1;j<=nv;j++) { 
				/* Propagate tableau 1 diagonal more. */
				q=d[j][k1];
				d[j][k1]=dy[j];
				delta=c[j]-q;
				dy[j]=f1*delta;
				c[j]=f2*delta;
				yz[j] += dy[j];
			}
		}
		for (j=1;j<=nv;j++) d[j][iest]=dy[j];
	}
	free_vector(c,1,nv);
}

void bsstep(State &s, double *xx, double htry, double eps, 
		double *hdid, double *hnext) {
	/* need indexing to start from 1, due to odd conventions in NR */
	int nv = n_vars(s);
	int nvbin = n_vars_binary(s);
	double *y = vector(1,nv);
	/*
	double yscal[NUMV+1] = { 1, 1, 1, 1, 1, 1, 1, 
				    1, 1, 1, 1, 1, 1, 1 };
				    */
	int erri; // index of maximum error
	int i,iq,k,kk,km;
	static int first=1,kmax,kopt;
	static double epsold = -1.0,xnew;
	double eps1,errmax,fact,h,red=1.0,scale=1.0,work,wrkmin,xest;
	double *err,*yerr,*ysav,*yseq;
	static double a[IMAXX+1];
	static double alf[KMAXX+1][KMAXX+1];
	// XXX: Can we include 1 step in the sequence? Try it so, leave out 18
	//static int nseq[IMAXX+1]={0,2,4,6,8,10,12,14,16,18};
	static int nseq[IMAXX+1]={0,1,2,4,6,8,10,12,14,16};
	int reduct,exitflag=0;

	/* marshal state to table y */
	bin2y(s, y);

	d=matrix(1,nv,1,KMAXX);
	err=vector(1,KMAXX);
	x=vector(1,KMAXX);
	yerr=vector(1,nv);
	ysav=vector(1,nv);
	yseq=vector(1,nv);
	if (eps != epsold) { /* A new tolerance, so reinitialize. */
		*hnext = xnew = -1.0e29; /*  "Impossible" values. */
		eps1=SAFE1*eps;
		a[1]=nseq[1]+1; /* Compute work coefficients Ak. */
		for (k=1;k<=KMAXX;k++) a[k+1]=a[k]+nseq[k+1];
		for (iq=2;iq<=KMAXX;iq++) { /* Compute alpha(k; q). */
			for (k=1;k<iq;k++)
				alf[k][iq]=pow(eps1,(a[k+1]-a[iq+1])/
						((a[iq+1]-a[1]+1.0)*(2*k+1)));
		}
		epsold=eps;
		/* Determine optimal row number for convergence. */
		for (kopt=2;kopt<KMAXX;kopt++) 
			if (a[kopt+1] > a[kopt]*alf[kopt-1][kopt]) break; 
		kmax=kopt;
	}
	h=htry;
	for (i=1;i<=nv;i++) ysav[i]=y[i]; /* Save the starting values.*/
	if (*xx != xnew || h != (*hnext)) { 
		/* A new stepsize or a new integration: re-establish the order
		 * window. */
		first=1; 
		kopt=kmax;
	}
	reduct=0;
	for (;;) {
		for (k=1;k<=kmax;k++) { 
			/* Evaluate the sequence of modified midpoint
			 * integrations. */
			xnew=(*xx)+h; 
			/* check that there are no fictious or real time
			 * underflows, though these only happen when
			 * using automatic step size control. */
			if (xnew == (*xx)) {
				fprintf(stderr, 
				"bsstep: step size underflow\n");
				exit(EXIT_FAILURE);
			}

			//bssubsteps(s, *xx, h, nseq[k], yseq);
			//fprintf(stderr, "substeps h %16.10g n %5d\n", h, nseq[k]);
			bssubsteps(s, h, nseq[k], yseq);
			if (yseq[1] == ysav[1]) {
				fprintf(stderr, 
				"bsstep: physical time underflow in bssubsteps\n");
				exit(EXIT_FAILURE);
			}
			/* Squared, since error series is even. */
			xest=SQR(h/nseq[k]); 
			/* Perform extrapolation. */
			pzextr(k,xest,yseq,y,yerr,nv); 
			if (k != 1) { 
				/* Compute normalized error estimate foo (k) */
				errmax=TINY;
				// XXX: Use constant 1 for all error
				// estimate scaling
				erri=0;
				// XXX: Only calculating error for
				// binary part of the state!
				//for (i=1;i<=nv;i++) {
				for (i=1;i<=nvbin;i++) {
					if (fabs(yerr[i]) > errmax) {
						// Scale with y
						errmax = fabs(yerr[i]/(y[i]+TINY));
						//errmax = fabs(yerr[i]);
						erri = i;
					}
					//errmax=FMAX(errmax, fabs(yerr[i]/yscal[i]));
				}
				/* Scale error relative to tolerance. */
				errmax /= eps; 
				km=k-1;
				err[km]=pow(errmax/SAFE1,1.0/(2*km+1));
                if (g_progconf.debug_enabled("bs")) 
                    fprintf(stderr, "k: %d erri: %d errmax: %g\n", k, erri, errmax);
			}
			if (k != 1 && (k >= kopt-1 || first)) { 
				/* In order window. */
				if (errmax < 1.0) { 
					/* Converged. */
					exitflag=1;
					break;
				}
				if (k == kmax || k == kopt+1) { 
					/* Check for possible stepsize
					 * reduction. */
					red=SAFE2/err[km]; 
					break;
				}
				else if (k == kopt 
					&& alf[kopt-1][kopt] < err[km]) {
					red=1.0/err[km];
					break;
				}
				else if (kopt == kmax 
						&& alf[km][kmax-1] < err[km]) {
					red=alf[km][kmax-1]*SAFE2/err[km];
					break;
				}
				else if (alf[km][kopt] < err[km]) {
					red=alf[km][kopt-1]/err[km];
					break;
				}
			}
		}
		if (exitflag) break;
		/* Reduce stepsize by at least REDMIN and at most REDMAX. */
		red=std::min(red,REDMIN);
		red=std::max(red,REDMAX);
		h *= red;
		reduct=1;
	} /* Try again. */

	/* Successful step taken. */
	*xx=xnew; 
	*hdid=h;
	first=0;
	/* Compute optimal row for convergence and corresponding stepsize. */
	wrkmin=1.0e35; 
	for (kk=1;kk<=km;kk++) { 
		fact=std::max(err[kk],SCALMX); 
		work=fact*a[kk+1];
		if (work < wrkmin) {
			scale=fact;
			wrkmin=work;
			kopt=kk+1;
		}
	}
	*hnext=h/scale;
	if (kopt >= k && kopt != kmax && !reduct) {
		/* Check for possible order increase, but not if stepsize was
		 * just reduced. */
		fact=std::max(scale/alf[kopt-1][kopt], SCALMX);
		if (a[kopt+1]*fact <= wrkmin) {
			*hnext=h/fact;
			kopt++;
		}
	}

	/* marshall results back into state */
	y2bin(y, s);
	free_vector(y,1,nv);
	free_vector(yseq,1,nv);
	free_vector(ysav,1,nv);
	free_vector(yerr,1,nv);
	free_vector(x,1,KMAXX);
	free_vector(err,1,KMAXX);
	free_matrix(d,1,nv,1,KMAXX);
}
