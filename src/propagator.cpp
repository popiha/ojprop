#include "propagator.hpp"
#include "system.hpp"
#include "pn.hpp"
#include "conf.hpp"
#include "units.hpp"
#include "globals.hpp"
#include "viscosity.hpp"
#include "misc.hpp"

#include <boost/foreach.hpp>

#include <iostream>
#include <fstream>
#include <utility>
#include <vector>
#include <algorithm>

#include <cstdio>

// BS-integrator
extern void bssubsteps(const State &sin, double t, double h, int nsteps, double out[]);
extern void bsstep(State &s, double *xx, double htry, double eps, double *hdid, double *hnext);

/*
void check_accretion(State &s);
void check_outliers(State &s);
*/
void check_accretion(State &s, disk &d);
void check_outliers(State &s, disk &d, MillerGrid &g);

double omega_disk(const State &s) {
	double om = 1/s.bin.r.norm();
	for (int i=0; i < s.disk1.n; i++)
		om += 1/(s.disk1.rs[i]-s.bin.rcm1).norm() 
			+ 1/(s.disk1.rs[i]-s.bin.rcm2).norm();
			//+ 1/(s.disk1.rs[i]-s.bin.r).norm();
	// TODO: Which is correct!
	return om;
}

double omega_binary(const State &s) {
	return 1/s.bin.r.norm();
}

//void disk_interaction_f(State &s, VectorXd &dv) {
void disk_interaction_f(const disk &d, MillerGrid &g, const disk_parameters &m, VectorXd &dv, const Vector3d &roff, const Vector3d &voff) {
	// A gridded viscosity calculation, calculating the deltav for
	// each particle due to differences from mean particle velocity
	if (!g_progconf.disk_interaction)
		return;

	// Iterate through grid cells, calculate mean motion and apply a
	// viscosity term relative to that mean motion

	// OpenMP parallelization should be safe, as every particle is
	// in only one grid cell at the most
#pragma omp parallel for shared(dv) if (g_progconf.use_openmp)
	for (int i=0; i < g.get_nr(); i++) {
		for (int j=0; j < g.get_nphi(); j++) {
			// Get indexes
			std::vector<int> &is = g[i][j];
			if (is.size() <= 0)
				continue;

			// Artificial viscosity mk. III
			// alpha * nu, with nu from Lehto & Valtonen 1996
			// Iterate over indexes and get mean velocity components
			// XXX: In LVLH frame!
			Vector3d mvel = Vector3d::Zero();
			int num=0;
			for (int k=0; k < (int)is.size(); k++) {
				if (d.states[is[k]] != disk::NORMAL) 
					continue;
				mvel += cart2lvlh(d.rs[is[k]]-roff, d.vs[is[k]]-voff);
				num++;
			}
			if (num != 0)
				mvel /= num;

			// Iterate over indexes and apply correction. For the
			// correction we use the constant value integrated over
			// the whole timestep
			// XXX: Velocity difference in LVLH frame!
			for (unsigned int k=0; k < is.size(); k++) {
				if (d.states[is[k]] != disk::NORMAL) 
					continue;
				double nu = viscosity_at((d.rs[is[k]]-roff).norm());
				// Currently take it from first disk
				double visco = m.alpha * nu;
				// We might want viscosity in only certain directions
				Vector3d diff = cart2lvlh(d.rs[is[k]]-roff, d.vs[is[k]]-voff) - mvel;
				// Take only z-component
				if (!m.visco_radial)
					diff[0] = 0;
				if (!m.visco_angular)
					diff[1] = 0;
				if (!m.visco_z)
					diff[2] = 0;
				Vector3d viscof = -visco*diff;
				// Project back to global cartesian coordinates
				viscof = lvlh2cart(d.rs[is[k]], viscof);
				if (g_progconf.debug_enabled("viscof")) {
					fprintf(stderr, "visco: %g vel: %g mvel: %g viscof: %g\n", 
						visco, d.vs[is[k]].norm(), mvel.norm(), viscof.norm());
				}
				// Return the contribution to existing forces
				dv.segment<3>(3*is[k]) = viscof;
			}
		}
	}
}

void check_accretion(State &s, disk &d) {
	// Return if accretion not enabled
	if (!g_progconf.disk_accretion)
		return;

	// Iterate through particles, and find and remove those inside
	// either accretion radius.
	double c1 = s.r_acc1*s.r_acc1;
	double c2 = s.r_acc2*s.r_acc2;

	std::vector<disk::ParticleState> &states = d.states;
#pragma omp parallel for shared(states) if (g_progconf.use_openmp)
	for (int i=0; i < d.n; i++) {
		double dist2; 
		if (states[i] != disk::NORMAL)
			continue;

		dist2 = (d.rs[i] - s.bin.rcm1).squaredNorm();
		if (dist2 <= c1) {
			// Accreted to primary
			if (d.infos[i] == disk::DISK_M1)
				s.stats.accretion_count[0]++;
			else
				s.stats.accretion_count[1]++;
			s.stats.accretion_total++;
			states[i] = disk::ACCRETED_M1;
		}
		else {
			dist2 = (d.rs[i] - s.bin.rcm2).squaredNorm();
			if (dist2 <= c2) {
				// Accreted to secondary
				if (d.infos[i] == disk::DISK_M1)
					s.stats.accretion_count[2]++;
				else
					s.stats.accretion_count[3]++;
				s.stats.accretion_total++;
				states[i] = disk::ACCRETED_M2;
			}
		}

		if (states[i] != disk::NORMAL) {
#pragma omp critical
			{
				g_particle_stream 
					<< s.physical_time()  << " " 
					<< d.ids[i] << " "
					<< (states[i] == disk::ACCRETED_M1 ?
						"ACCRETED_M1" : "ACCRETED_M2")
					<< "\n";
			}
		}
	}
}

void check_inclination(State &s) {
	// Return if escape by inclination is not enabled
	if (!g_progconf.disk_inclination_escape)
		return;

	// Iterate through particles, and find those whose 
	// inclination is larger than set limit, 
	// and are in the z > 0 half-space

	const double mu = C_G * s.m.m1;
	const double limit = s.m.inclination_limit * M_PI/180.0;
	std::vector<disk::ParticleState> &states = s.disk1.states;

#pragma omp parallel for shared(states) if (g_progconf.use_openmp)
	for (int i=0; i < s.disk1.n; i++) {
		if (states[i] != disk::NORMAL)
			continue;

		Vector3d pos = s.disk1.rs[i] - s.bin.rcm1;
		Vector3d vel = s.disk1.vs[i] - s.bin.vcm1;
		double oe[6] = {0};
		rv2oe_gp(mu, pos, vel, oe);

		// Reduce to [0, pi/2)
		if (oe[2] > M_PI/2)
			oe[2] = M_PI - oe[2];

		// MK1 :
		// Check inclination against scale height and radial distance.
		// Use maximal z-separation as the criterion. We get this from inclination & semimajor axis & eccentricity.
		// Also only report particles from the side of the disk facing the observer.
		// Stddev in z is pi * z0/(2 * sqrt(3)). Take the limit as inc_limit * stddev
		double stddev = M_PI*s.m.disk1.scale_height/(2 * sqrt(3.0));
		double zmax = s.m.inclination_limit * stddev;
		double rmax = oe[0]*(1+oe[1]); // rmax = a*(1+ecc)
		double maxinc = atan2(zmax, rmax);
		//if (fabs(oe[2]) > maxinc && pos[2] > 0) {

		// MK2: Just check the z component
		if (pos[2] > zmax) {
			if (g_progconf.debug_enabled("inclination")) {
#pragma omp critical 
				{
					g_debug_stream << "Inclination escape, a " << oe[0] << " e " << oe[1] 
						<< " i " << oe[2] << " zmax " << zmax << " rmax " << rmax
						<< " r " << sqrt(pos[0]*pos[0]+pos[1]*pos[1]) << " z " << pos[2]
						<< std::endl;
				}
			}
			// A particle escaped through inclination
			states[i] = disk::ESCAPED_INCLINATION;
			s.stats.inclination_escape_count++;
			s.stats.escape_total++;
#pragma omp critical
			{
				g_particle_stream 
					<< s.physical_time()  << " " 
					<< s.disk1.ids[i] << " "
					<< "ESCAPED_INCLINATION"
                    << " r = " << pos.norm()
					<< "\n";
			}
		}
	}
}

void check_outliers(State &s, disk &d, MillerGrid &g) {
	// Get current outliers
	std::vector<int> &out = g.outside();
	size_t n = out.size();

	// Compare to what we know
	for (unsigned int i=0; i < n; i++) {
		if (d.states[out[i]] == disk::NORMAL) {
			// New outlier, escaped to RMAX (RMIN should lie inside accretion radius)
			d.states[out[i]] = disk::ESCAPED_RMAX;
			// Increment escape statistics counter
			s.stats.escape_total++;

			// Write the event into particle info stream
			g_particle_stream 
				<< s.physical_time()  << " " << d.ids[out[i]] << " "
				<< "ESCAPED"
				<< " r = " << (d.rs[out[i]]-g.position).norm()
				<< "\n";
		}
	}
}

void propagate(State &s, double timestep) {
	// Propagate with Bulirsch-Stoer
	static double t = 0, hdid=0, hnext=timestep;

	// Recalculate TTL w
	if (g_progconf.use_TTL) {
		double new_w = omega_binary(s);
		s.ttlw = new_w;
	}

	// Mikkola's esoteric timestep selection?
	// Mikkola has velocities divided by sqrt(W) in timestep selection, so do same
#if 0
	double ordv = ((1/sqrt(s.ttlw))*s.bin.v).array().abs().sum();
	debug_print(stderr, "ordv", ordv);
	debug_print(stderr, "sm", s.pc_bin.m);
	debug_print(stderr, "W", s.ttlw);
	debug_print(stderr, "Mikkola: .3/(ordv+sqrt(sm*W)/NB)", 0.3/(ordv + sqrt(s.pc_bin.m*s.ttlw)/2));

	hnext = std::min(hnext, 0.3/(ordv + sqrt(s.pc_bin.m*s.ttlw)/2));
	debug_print(stderr, "Mikkola selection: new hnext", hnext);
#endif

	// Cap timestep
	hnext = std::min(hnext, g_progconf.max_timestep);
	hnext = std::max(hnext, g_progconf.min_timestep);

	//fprintf(stderr, "stepping with %16.10e\n", hnext);

	// Make one step
	bsstep(s, &t, hnext, g_progconf.bs_epsilon, &hdid, &hnext);		

	/*
	fprintf(stderr, "hdid %16.10e hnext %16.10e\n", hdid, hnext);
	fprintf(stderr, "t %16.10e s.t %16.10e\n", t, s.t);
	*/

	//
	// Now update every dependent value that is needed for any
	// purpose before the beginning of the next step, or in the
	// next step

	// Update rcm positions and velocities
	s.update_rcm();

	// Update grid to reflect new positions
	//s.grid.update(s.disk);
	g_grid1.update(s.disk1.rs);
	g_grid2.update(s.disk2.rs);

	// Check for accretion
	check_accretion(s, s.disk1);
	check_accretion(s, s.disk2);

	// Check for grid outliers
	check_outliers(s, s.disk1, g_grid1);
	check_outliers(s, s.disk2, g_grid2);

	// Check inclination escapes for disk1
	check_inclination(s);

	// Recalculate keplerian energy.
	s.update_constants();

	// Update gridcell statistics
	s.update_grid_stats();
}

