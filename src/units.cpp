#include "units.hpp"

const double A = 365.25 * 86400.0;
const double AU = 149597870691.0;
const double MS = 1.98892e30;

const double YEAR_IN_SECONDS = A;
const double AU_IN_METERS = AU;
const double SOLAR_MASS_IN_KILOGRAMS = MS;

const double C_IAU_G = 6.67428e-11*A*A*MS/(AU*AU*AU);
const double C_IAU_c = 63241.07708809;
const double C_IAU_G_PER_c2 = C_IAU_G/(C_IAU_c*C_IAU_c);

// These will be derived from configuration data.
double C_G = C_IAU_G, C_c = C_IAU_c;


