#include "conf.hpp"
#include "misc.hpp"

#include <boost/property_tree/ptree.hpp>
#include <boost/foreach.hpp>
#include <exception>
#include <iostream>
#include <string>
#include <set>
#include <cmath>


void Configuration::sync_ptree() {
	using boost::property_tree::ptree;

	pt.clear();
	pt.put("conf.start_year", start_year);
	pt.put("conf.end_year", end_year);
	pt.put("conf.max_timestep", max_timestep);
	pt.put("conf.min_timestep", min_timestep);
	pt.put("conf.print_progress", print_progress);
	pt.put("conf.print_interval", print_interval);
	pt.put("conf.print_disk", print_disk);
	pt.put("conf.disk_interval", disk_interval);
	pt.put("conf.disk_print_distance", disk_print_distance);
	pt.put("conf.unit_G", unit_G);
	pt.put("conf.use_TTL", use_TTL);
	pt.put("conf.use_disk_m1_PN", use_disk_m1_PN);
	pt.put("conf.use_disk_m2_PN", use_disk_m2_PN);
	pt.put("conf.disk_interaction", disk_interaction);
	pt.put("conf.disk_snapshots", disk_snapshots);
	pt.put("conf.disk_accretion", disk_accretion);
	pt.put("conf.inclination_escape", disk_inclination_escape);
	pt.put("conf.fast_disk", fast_disk);
	pt.put("conf.use_openmp", use_openmp);

	pt.put("conf.limits.cut_epsilon", cut_epsilon);
	pt.put("conf.limits.cut_max_it", cut_max_it);
	pt.put("conf.limits.pn_epsilon", pn_epsilon);
	pt.put("conf.limits.pn_max_it", pn_max_it);
	pt.put("conf.limits.bs_epsilon", bs_epsilon);

	BOOST_FOREACH(const std::string &name, tweaks)
		pt.add("conf.compatibility", name);

	pt.put("conf.postnewtonian.PN1", PN1);
	pt.put("conf.postnewtonian.PN2", PN2);
	pt.put("conf.postnewtonian.PN2_5", PN2_5);
	pt.put("conf.postnewtonian.PN3", PN3);
	pt.put("conf.postnewtonian.PN3_5", PN3_5);
	pt.put("conf.postnewtonian.spin-orbital", PN_spinorbital);
	pt.put("conf.postnewtonian.quadrupole", PN_quadrupole);
	pt.put("conf.postnewtonian.spindot", PN_spindot);

	BOOST_FOREACH(const std::string &name, debug_modules)
		pt.add("conf.debug_modules.module", name);

	pt.put("conf.debug_file", debug_file);
	pt.put("conf.orbit_file", orbit_file);
	pt.put("conf.cut_file", cut_file);
	pt.put("conf.disk_file", disk_file);
	pt.put("conf.accretion_file", accretion_file);
	pt.put("conf.particle_file", particle_file);
	pt.put("conf.stat_file", stat_file);
	pt.put("conf.element_file", element_file);
}

void Configuration::sync_instance() {
	using boost::property_tree::ptree;

	// Read variables from property tree into class
	start_year 			= pt.get<double>("conf.start_year");
	end_year 			= pt.get<double>("conf.end_year");
	max_timestep 		= pt.get<double>("conf.max_timestep");
	min_timestep 		= pt.get<double>("conf.min_timestep");
	disk_print_distance = pt.get<double>("conf.disk_print_distance");
	print_progress 		= pt.get<bool>("conf.print_progress");
	print_interval 		= pt.get<double>("conf.print_interval");
	print_disk			= pt.get<bool>("conf.print_disk");
	disk_interval		= pt.get<double>("conf.disk_interval");
	unit_G 				= pt.get<bool>("conf.unit_G");
	use_TTL 			= pt.get<bool>("conf.use_TTL");
	use_disk_m1_PN 		= pt.get<bool>("conf.use_disk_m1_PN");
	use_disk_m2_PN 		= pt.get<bool>("conf.use_disk_m2_PN");
	disk_interaction 	= pt.get<bool>("conf.disk_interaction");
	disk_snapshots 		= pt.get<bool>("conf.disk_snapshots");
	disk_accretion 		= pt.get<bool>("conf.disk_accretion");
	disk_inclination_escape	= pt.get<bool>("conf.disk_inclination_escape");
	fast_disk			= pt.get<bool>("conf.fast_disk");
	use_openmp			= pt.get<bool>("conf.use_openmp");

	cut_epsilon 		= pt.get<double>("conf.limits.cut_epsilon");
	cut_max_it 			= pt.get<double>("conf.limits.cut_max_it");
	pn_epsilon 			= pt.get<double>("conf.limits.pn_epsilon");
	pn_max_it 			= pt.get<double>("conf.limits.pn_max_it");
	bs_epsilon 			= pt.get<double>("conf.limits.bs_epsilon");

	BOOST_FOREACH(ptree::value_type &v,
			pt.get_child("conf.compatibility"))
		tweaks.insert(v.first.data());

	PN1 		= pt.get<bool>("conf.postnewtonian.PN1");
	PN2 		= pt.get<bool>("conf.postnewtonian.PN2");
	PN2_5 		= pt.get<bool>("conf.postnewtonian.PN2_5");
	PN3 		= pt.get<bool>("conf.postnewtonian.PN3");
	PN3_5 		= pt.get<bool>("conf.postnewtonian.PN3_5");
	PN_spinorbital 	= pt.get<bool>("conf.postnewtonian.spin-orbital");
	PN_quadrupole 	= pt.get<bool>("conf.postnewtonian.quadrupole");
	PN_spindot 	= pt.get<bool>("conf.postnewtonian.spindot");

	BOOST_FOREACH(ptree::value_type &v,
			pt.get_child("conf.debug_modules"))
		debug_modules.insert(v.second.data());

	debug_file 	= pt.get<std::string>("conf.debug_file");
	orbit_file 	= pt.get<std::string>("conf.orbit_file");
	cut_file 	= pt.get<std::string>("conf.cut_file");
	disk_file 	= pt.get<std::string>("conf.disk_file");
	accretion_file 	= pt.get<std::string>("conf.accretion_file");
	particle_file 	= pt.get<std::string>("conf.particle_file");
	stat_file = pt.get<std::string>("conf.stat_file");
	element_file = pt.get<std::string>("conf.element_file");
}

//void Configuration::default_configuration() {
void Configuration::defaults() {
	start_year = 1855.89679;
	end_year = 2050.0;
	max_timestep = 0.01;
	print_interval = 0.1;
	unit_G = false;
	use_TTL = false;
	disk_snapshots = true;
	disk_accretion = true;

	cut_epsilon = 1e-10;
	cut_max_it = 1000;
	pn_epsilon = 1e-10;
	pn_max_it = 1000;

	debug_file.assign("debug.log");
	debug_modules.insert("main");
}

bool Configuration::tweak_enabled(const std::string &term) {
	return (tweaks.find(term) != tweaks.end());
}

bool Configuration::debug_enabled(const std::string &module_name) {
	return (debug_modules.find(module_name) != debug_modules.end());
}


void Model::sync_ptree() {
	using boost::property_tree::ptree;

	pt.put("model.semimajor_axis", a);
	pt.put("model.eccentricity", ecc);
	pt.put("model.apocentre_theta", apo_theta);
	pt.put("model.spin_magnitude", spin_chi);
	pt.put("model.spin_theta", spin_theta);
	pt.put("model.spin_phi", spin_phi);
	pt.put("model.mass1", m1);
	pt.put("model.mass2", m2);
	pt.put("model.time_delay", td);
	pt.put("model.redshift", z);
	pt.put("model.no_hair_parameter", q);
	pt.put("model.accretion_radius_1", r_acc1);
	pt.put("model.accretion_radius_2", r_acc2);
	pt.put("model.inclination_limit", inclination_limit);

	pt.put("model.disk1.particles", disk1.n_parts);
	pt.put("model.disk1.inner_radius", disk1.r_inner);
	pt.put("model.disk1.outer_radius", disk1.r_outer);
	pt.put("model.disk1.scale_height", disk1.scale_height);
	pt.put("model.disk1.alpha", disk1.alpha);
	pt.put("model.disk1.visco_radial", disk1.visco_radial);
	pt.put("model.disk1.visco_angular", disk1.visco_angular);
	pt.put("model.disk1.visco_z", disk1.visco_z);
	pt.put("model.disk1.grid.phi_cells", disk1.n_grid_phi);
	pt.put("model.disk1.grid.r_cells", disk1.n_grid_r);
	pt.put("model.disk1.grid.max_r", disk1.max_grid_r);

	pt.put("model.disk2.particles", disk2.n_parts);
	pt.put("model.disk2.inner_radius", disk2.r_inner);
	pt.put("model.disk2.outer_radius", disk2.r_outer);
	pt.put("model.disk2.scale_height", disk2.scale_height);
	pt.put("model.disk2.alpha", disk2.alpha);
	pt.put("model.disk2.visco_radial", disk2.visco_radial);
	pt.put("model.disk2.visco_angular", disk2.visco_angular);
	pt.put("model.disk2.visco_z", disk2.visco_z);
	pt.put("model.disk2.grid.phi_cells", disk2.n_grid_phi);
	pt.put("model.disk2.grid.r_cells", disk2.n_grid_r);
	pt.put("model.disk2.grid.max_r", disk2.max_grid_r);
}

void Model::sync_instance() {
	using boost::property_tree::ptree;

	// Update variables
	a = pt.get<double>("model.semimajor_axis");
	ecc = pt.get<double>("model.eccentricity");
	apo_theta = pt.get<double>("model.apocentre_theta");
	spin_chi = pt.get<double>("model.spin_magnitude");
	spin_theta = pt.get<double>("model.spin_theta");
	spin_phi = pt.get<double>("model.spin_phi");
	m1 = pt.get<double>("model.mass1");
	m2 = pt.get<double>("model.mass2");
	td = pt.get<double>("model.time_delay");
	z = pt.get<double>("model.redshift");
	q = pt.get<double>("model.no_hair_parameter");
	r_acc1 = pt.get<double>("model.accretion_radius_1");
	r_acc2 = pt.get<double>("model.accretion_radius_2");
	inclination_limit = pt.get<double>("model.inclination_limit");

	disk1.n_parts = pt.get<int>("model.disk1.particles");
	disk1.r_inner= pt.get<double>("model.disk1.inner_radius");
	disk1.r_outer= pt.get<double>("model.disk1.outer_radius");
	disk1.scale_height = pt.get<double>("model.disk1.scale_height");
	disk1.alpha = pt.get<double>("model.disk1.alpha");
	disk1.visco_radial = pt.get<bool>("model.disk1.visco_radial");
	disk1.visco_angular = pt.get<bool>("model.disk1.visco_angular");
	disk1.visco_z = pt.get<bool>("model.disk1.visco_z");
	disk1.n_grid_phi = pt.get<int>("model.disk1.grid.phi_cells");
	disk1.n_grid_r = pt.get<int>("model.disk1.grid.r_cells");
	disk1.max_grid_r = pt.get<double>("model.disk1.grid.max_r");

	disk2.n_parts = pt.get<int>("model.disk2.particles");
	disk2.r_inner= pt.get<double>("model.disk2.inner_radius");
	disk2.r_outer= pt.get<double>("model.disk2.outer_radius");
	disk2.scale_height = pt.get<double>("model.disk2.scale_height");
	disk2.alpha = pt.get<double>("model.disk2.alpha");
	disk2.visco_radial = pt.get<bool>("model.disk2.visco_radial");
	disk2.visco_angular = pt.get<bool>("model.disk2.visco_angular");
	disk2.visco_z = pt.get<bool>("model.disk2.visco_z");
	disk2.n_grid_phi = pt.get<int>("model.disk2.grid.phi_cells");
	disk2.n_grid_r = pt.get<int>("model.disk2.grid.r_cells");
	disk2.max_grid_r = pt.get<double>("model.disk2.grid.max_r");
}

void Model::defaults() {
	a = 11507.957168890862;
	ecc = 0.6577;
	apo_theta = 56.39*M_PI/180;
	spin_chi = 0.27544;
	spin_theta = 3.0394;
	spin_phi = -1.6563; 
	m1 = 1.83e10; 
	m2 = 1.4e8;
	td = 0.739;	
	z = 0.3;
	q = 1.0;
}
