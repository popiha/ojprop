#ifdef _WIN32
#define WINDOWS_LEAN_AND_MEAN
#define NOMINMAX
#include <Windows.h>
#endif

#include "globals.hpp"
#include "conf.hpp"
#include "system.hpp"
#include "propagator.hpp"
#include "units.hpp"
#include "misc.hpp"
#include "interpolation.hpp"
#include "viscosity.hpp"

#include <omp.h>
#include <boost/foreach.hpp>
#include <boost/circular_buffer.hpp>

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstring>
#include <csignal>
#include <map>
#include <stdlib.h>
#include <chrono>
#include <ctime>

#ifdef USE_SDL
#include "sdlmain.hpp"
#include <SDL.h>
#endif

// Eclipse workaround for BOOST_FOREACH parsing problems
#ifdef __CDT_PARSER__
#define BOOST_FOREACH(a, b) for(a : b)
#endif

// Function prototypes
typedef void (*sighnd)(int);
void signal_handler(int sig); 
void disk_snapshot(State &s); 
void disk_snapshot(State &s, int i); 
void load_from_file(PropertyTreeContainer &cnt, std::string fname);
void debug_OMP();
int debug_main(Model &m);
//int debug_cuda_main();
bool detect_cut(double curr_pcsec , double prev_pcsec) { return curr_pcsec*prev_pcsec < 0; }
void handle_cut(const State &s_old, const State &s, double h); 
//void interp_cut(State &s, State &prev_s, double pc, double prev_pc); 
double poincare_section(const State &s);

// Struct to hold values for interpolation when a cut is detected
struct interpolates {
	double phys_t;
	double r;
	double z;
	double vz;
	double psec;
	void calculate(const State &s) {
		phys_t = s.physical_time();
		r = s.bin.r.segment<2>(0).norm();
		z = s.bin.r.z();
		vz = s.bin.v.z();
		psec = poincare_section(s);
	}
};

// Length of the interpolation buffer
const int IBUF_LENGTH = 5;

void interp_cut(struct interpolates *ibuf);

int main(int argc, char **argv) {

	// Load configuration first, so that we can setup all streams, incl. debug stream
	load_from_file(g_progconf, "default.conf");

	// Open output streams
	open_streams();

	// Print OMP debuginformation
	debug_OMP();

	// Register signal handlers
    // XXX: Not a good idea to ignore INT and TERM signals
#if 0
	sighnd prev_sighnd;
	prev_sighnd = signal(SIGINT, signal_handler);
	if (prev_sighnd == SIG_IGN)
		signal(SIGINT, SIG_IGN);
	signal(SIGTERM, signal_handler);
	if (prev_sighnd == SIG_IGN)
		signal(SIGTERM, SIG_IGN);
#endif

	// Load model and store to instance oj_model
	Model oj_model;
	load_from_file(oj_model, "default.model");

	// Adjust numerical values of physical constants based on the configuration
	if (g_progconf.unit_G) {
		C_G = 1.0;
		C_c = C_IAU_c / sqrt(C_IAU_G);
	}
	// XXX: Valtonen/Mikkola uses a system where c = 10000, G = 1.
	// Here we make sure that everything works even if G != 1, and
	// thus the sqrt(G) -term
	if (g_progconf.tweak_enabled("lightspeed"))
		C_c = 10000 * sqrt(C_G);

	// Generate an initial state from the model and configuration,
	// and output it to orbit data stream
	g_state.reconstruct(oj_model);
	/*
	g_grid.initialize(oj_model.disk.n_grid_r, oj_model.disk.n_grid_phi, oj_model.disk.max_grid_r);
	g_grid.update(g_state.disk.rs);
	*/
	g_grid1.initialize(oj_model.disk1.n_grid_r, oj_model.disk1.n_grid_phi, oj_model.disk1.max_grid_r, g_state.bin.rcm1);
	g_grid1.update(g_state.disk1.rs);
	g_grid1.position = g_state.bin.rcm1;

	g_grid2.initialize(oj_model.disk2.n_grid_r, oj_model.disk2.n_grid_phi, oj_model.disk2.max_grid_r, g_state.bin.rcm2);
	g_grid2.update(g_state.disk2.rs);
	g_grid2.position = g_state.bin.rcm2;

	State &s = g_state;
	g_orbit_stream << s.bin_to_string() << std::endl;
	disk_snapshot(s);
	
	// When we last output data
	double last_print = g_progconf.start_year;
	double last_disk  = g_progconf.start_year;

	if (g_progconf.debug_enabled("main")) {
		// Print out what we read in
		debug_main(oj_model);
	}

	// Calculate timestep as a fraction of the (initial) period
	double g = C_G;
	// DEBUG: try to match Mikkola timestepping
	double timestep = g_progconf.max_timestep /* * 
		sqrt(4*M_PI*M_PI/(g*(oj_model.m1+oj_model.m2))
					*pow(oj_model.a, 3))*/;
	if (g_progconf.debug_enabled("main")) {
		g_debug_stream << "timestep = " << timestep << std::endl;
	}


	// Get time elapsed for distant observer and use it as a
	// reference for everything in propagation loop
	double phys_t = s.physical_time();

	// Store previous state and two values of poincare section for interpolation
	State prev_state;
	prev_state = s;
	double curr_pcsec = poincare_section(s);
	double prev_pcsec = curr_pcsec;
	// Also store z-component of secondary rcm coordinates
	double curr_rcm2z = s.bin.rcm2[2];
	double prev_rcm2z = curr_rcm2z;

	// Initialize Time Transformed Leapfrog
	if (g_progconf.use_TTL) {
		// Calculate initial time transformation variable value
		s.ttlw = omega_binary(s);
		if (g_progconf.debug_enabled("TTL"))
			g_debug_stream << "w = " << s.ttlw << std::endl;
		// If TTL is enabled, scale so that initial timesteps at
		// apocentre agree
		// DEBUG: try to match Mikkola timestepping
		//timestep *= s.ttlw;
	}

	// Initialize viscosity calculation from Lehto & Valtonen 1996
	viscosity_precalc();

#ifdef USE_SDL
	// Init SDL screen
	sdl_init();
#endif

	// Create a circular buffer for the interpolates
	interpolates ibuf[IBUF_LENGTH];
	memset(&ibuf, 0, sizeof(interpolates)*IBUF_LENGTH);
	/*
	boost::circular_buffer<State> statebuffer(1);
	statebuffer.push_back(s);
	*/

	// Store time for printing out simulation duration
	std::time_t start_time;
	std::time(&start_time);

    std::cout << "Simulation starting at " << std::ctime(&start_time) << std::endl;

	//
	// Main propagation loop
	int disk_snap=0;
	for (;;) {
#ifdef USE_SDL
		// Run SDL graphics loop
		sdl_graphics_loop();
#endif
		// Stop integration if we've reached endpoint
		if (phys_t >= g_progconf.end_year)
			break;

		// Propagate system, calculate new physical time
		propagate(s, timestep);
		phys_t = s.physical_time();

		// We wish to interpolate after a cut to find precise
		// figures for time and other interesting variables, so
		// we need to keep a buffer of those values.
		// Keep buffer ordered, store in last position always.
		memmove(ibuf, ibuf+1, sizeof(interpolates)*(IBUF_LENGTH-1));
		ibuf[IBUF_LENGTH-1].calculate(s);

		// Store new values
		curr_pcsec = poincare_section(s);
		curr_rcm2z = s.bin.rcm2[2];

#if 0
		// Output disk data before and after impact
		{
			double dist = g_progconf.disk_print_distance;
			double zabs = std::fabs(curr_rcm2z);
			double prev_zabs = std::fabs(prev_rcm2z);
			if ((zabs < dist && prev_zabs > dist)
				|| (zabs > dist && prev_zabs < dist)) { 
					fprintf(stderr, "Secondary near disk, outputting disk data.\n");
					disk_snapshot(s, disk_snap++);
			}
		}
#endif

		// Output orbit and accretion data if the output interval has passed
		if (last_print + g_progconf.print_interval < phys_t) {
		// Mikkola's doesn't use physical time
		//if (last_print + 0.005 < s.t) {
			// Output orbit data
			g_orbit_stream << s.bin_to_string() << std::endl;

			// Output orbital element statistics
			s.update_element_stats();
			g_element_stream << s.disk_oe_stats_to_string() << std::endl;


			// Output accumulating stats
			g_accretion_stream << s.stat_accumulators_to_string() << std::endl;
			s.clear_stat_accumulators();

			if (g_progconf.print_progress) {
                std::time_t time_now = std::time(nullptr);
                std::tm tm = *std::localtime(&time_now);
                double elapsed = std::difftime(time_now, start_time);
                double amount_complete = (phys_t-g_progconf.start_year) / (g_progconf.end_year-g_progconf.start_year);
                double completion_estimate = elapsed / amount_complete; // in seconds
                std::cout << "Progress " << 100*amount_complete << "%, sim time " << phys_t 
                    << ", time " << std::put_time(&tm, "%F %T")
                    << ", estimated completion time " << completion_estimate / (3600) << " h" << std::endl;
				//printf("Progress: %f%%, physical time %f\n", 100*amount_complete, phys_t);
			}

			// Explicitly streams to make sure freshest data possible was written
			flush_streams();

			last_print = phys_t;
			//last_print = s.t;
		}

		// Output disk data if the interval has passed
		if (last_disk + g_progconf.disk_interval < phys_t) {
			disk_snapshot(s, ++disk_snap);
			last_disk = phys_t;
		}

		// Find all intersections with the disk and use secant
		// method to converge exactly to the disk. This is
		// analogous to Poincaré cuts.
		if (detect_cut(curr_pcsec, prev_pcsec)) {

			// Cut found
			if (g_progconf.debug_enabled("main"))
				g_debug_stream << "Cut found! t = " << s.physical_time() << "\n";

			// Print disk data for this cut
			disk_snapshot(s);

			// DEBUG: don't do anything
#if 0
			interp_cut(ibuf);
#endif
		}

		// Store state in the circular buffer
		//statebuffer.push_back(s);
		prev_state = s;
		prev_pcsec = curr_pcsec;
		prev_rcm2z = curr_rcm2z;
	}

	// Simulation done. Print statistics about the run
	{
		g_debug_stream << "Simulation done." << std::endl;
		std::cout << "Simulation done." << std::endl;
		// Time spent
		{
			std::stringstream sb;
			std::time_t end_time;
			time(&end_time);
			double sec = std::difftime(end_time, start_time);
			double min = sec/60;
			double hr = sec/3600;
			sb << "Took " << sec << " s = " << min << " m = " << hr << " h";
			g_debug_stream << sb.str() << std::endl;
			std::cout << sb.str() << std::endl;
		}
		// Particle escapes & accretions
		{
			std::stringstream sb;
			sb	<< "Particles: total / escaped / accreted:\n" << s.disk1.n+s.disk2.n 
				<< " " << s.stats.escape_total << " " << s.stats.accretion_total;
			g_debug_stream << sb.str() << std::endl;
			std::cout << sb.str() << std::endl;
		}
	}
 
	// Close streams and return success
	close_streams();
	return 0;
}

// Handler to flush output streams when terminated with a signal
void signal_handler(int sig) {
	// Flush and close all streams
	flush_streams();
	close_streams();
	exit(EXIT_FAILURE);
}

// Output a snapshot of the disk particles and disk statistics
void disk_snapshot(State &s, int i) {
	// Create filename from template and current time
	std::stringstream ss;
	if (i >= 0)
		ss << i << ".out";
	else
		ss << s.physical_time() << ".out";

	std::ofstream ssfile(("disk_" + ss.str()).c_str());
	ssfile << s.disk_to_string();
	ssfile.close();

#if 0
	// If we were called with index number for plotting purposes, add the orbit data
	if (i >= 0)
		ssfile << "\n\n" << s.bin_to_string();
#endif
	ssfile.close();

	// So far, no use for radial stats.
#if 0
	ssfile.open(("disk_stats_" + ss.str()).c_str());
	ssfile << s.disk_radstats_to_string();
	ssfile.close();
#endif
}

void disk_snapshot(State &s) {
	disk_snapshot(s, -1);
}

double poincare_section(const State &s) {
	int ri, pi;
	if (!g_grid1.index2(s.bin.rcm2, ri, pi)) {
		//fprintf(stderr, "binary not within grid, rcm2.z %g\n", s.bin.rcm2[2]);
		return s.bin.rcm2[2];
	}
	double zmed = s.stats.cell[ri][pi].z_median;
	//fprintf(stderr, "bin z %g disk z %g pc %g\n", s.bin.rcm2[2], zmed, s.bin.rcm2[2] - zmed);
	return s.bin.rcm2[2] - zmed;
}

void handle_cut(const State &s_old, const State &s, double h) {
	// Skip cut
#if 0
	// Make a copy of the state
	State cs = s_old;

	// Use secant method to find the cut
	double h0 = 0, h1 = h, h2 = 0;
	double f0 = s_old.r[2], f1 = s.bin.r[2];
	if (g_progconf.use_TTL)
		h1 /= g_ttl_w;

	int it=0;
	for (;;) {

		// New estimate for h
		h2 = h1 - f1 * (h1 - h0)/(f1 - f0);


		// Reset the copied state to original
		//memcpy(&cs, &s, sizeof(state));
		cs = s_old;

		// Propagate it with the new timestep
		propagate(cs, h2);

		// Recheck z-component
		if (fabs(cs.bin.r[2]) < g_progconf.cut_epsilon) {
			g_debug_stream << "Cut converged. "
				<< "it = " << it
				<< " err = " << fabs(cs.bin.r[2]) << "\n";
			break;
		}
		if (++it >= g_progconf.cut_max_it) {
			g_debug_stream << "Cut iterations exceeded. "
				<< "it = " << it
				<< " err = " << fabs(cs.bin.r[2]) << "\n";
			break;
		}

		// Update variables and reiterate
		h0 = h1;
		h1 = h2;
		f0 = f1;
		f1 = cs.bin.r[2];
	}

	// At the cut or near it. Output to stream
	g_cut_stream << cs.bin_to_string() << "\n";
#else
	// TODO: With modifications to state, the above iteration ends
	// up affecting the original state for some odd reason. Also,
	// copying the state is probably not a good idea, due to
	// (possibly large) number of disk particles
	g_cut_stream << s.bin_to_string() << "\n";
#endif
}

#if 0
void interp_cut(State &s, State &sp, double pc, double pcp) {
	// our x-value is the value of the poincare section, y values differ
	double sec1 = pc;
	double sec2 = pcp;
	double r1 = s.bin.rcm2.segment<2>(0).norm();
	double r2 = sp.bin.rcm2.segment<2>(0).norm();

	// interpolate to value zero of the poincare section
	double t = lin_interp(0, sec1, sec2, s.physical_time(), sp.physical_time());
	double r = lin_interp(0, sec1, sec2, r1, r2);
	double z = lin_interp(0, sec1, sec2, s.bin.rcm2[2], sp.bin.rcm2[2]);
	g_stat_stream 
		<< std::scientific << std::setprecision(15) 
		<< t << " " << r << " " << z << std::endl;
}
#endif
void interp_cut(struct interpolates *ibuf) {
	// our x-value is the value of the poincare section, y values vary
	double xbuf[IBUF_LENGTH];
	double tbuf[IBUF_LENGTH];
	double rbuf[IBUF_LENGTH];
	double zbuf[IBUF_LENGTH];
	double vzbuf[IBUF_LENGTH];

	for (int i=0; i < IBUF_LENGTH; i++) {
		xbuf[i] = ibuf[i].psec;
		tbuf[i] = ibuf[i].phys_t;
		rbuf[i] = ibuf[i].r;
		zbuf[i] = ibuf[i].z;
		vzbuf[i] = ibuf[i].vz;
	}
	if (g_progconf.debug_enabled("main")) {
		g_debug_stream << "Cut interpolation, buffer contents: (psec, t, r, z, vz)\n";
		for (int i=0; i < IBUF_LENGTH; i++) {
			g_debug_stream
				<< xbuf[i] << " "
				<< tbuf[i] << " "
				<< rbuf[i] << " "
				<< zbuf[i] << " "
				<< vzbuf[i] << "\n";
		}
	}

	// Interpolate each value
	const int order = IBUF_LENGTH-1;
	fh_precalc pc;
	fh_precalculate(&pc, xbuf, tbuf, order);
	double cut_t = fh_interp(&pc, 0);

	fh_reassign(&pc, xbuf, rbuf);
	double cut_r = fh_interp(&pc, 0);

	fh_reassign(&pc, xbuf, zbuf);
	double cut_z = fh_interp(&pc, 0);
	
	fh_reassign(&pc, xbuf, vzbuf);
	double cut_vz = fh_interp(&pc, 0);
	
	fh_free(&pc);

	g_stat_stream 
		<< std::scientific << std::setprecision(15) 
		<< cut_t << " " << cut_r << " " << cut_z << " " << cut_vz << std::endl;
}

void debug_OMP() {
	g_debug_stream << "OMP: debugging information\n";
#pragma omp parallel if (g_progconf.use_openmp)
	{
#pragma omp critical(debugprint)
		g_debug_stream << "OMP: thread no. " << omp_get_thread_num() << " reporting\n";
#pragma omp barrier
#pragma omp master
		g_debug_stream << "OMP: number of threads " << omp_get_num_threads() << "\n";
	}
}

void load_from_file(PropertyTreeContainer &cnt, std::string fname) {
	try {
		cnt.load(fname);
	}
	catch(std::exception &e) {
		std::cerr << "Error loading " << fname << ":" << e.what() << "\n";
		std::exit(1);
		//cnt.defaults();
	}
}

int debug_main(Model &m) {
	g_debug_stream << "conf:\n" << g_progconf.to_string() << "\n";
	g_debug_stream << "model:\n" << m.to_string() << "\n";
	g_debug_stream << "state:\n" << g_state.bin_to_string() << "\n";

	g_debug_stream << "debugged modubles: ";
	BOOST_FOREACH(const std::string &name, g_progconf.debug_modules)
		g_debug_stream << name << " ";
	g_debug_stream << "\n";

	g_debug_stream << "enabled PN-modules: ";
	/*
	BOOST_FOREACH(const std::string &name, g_progconf.pn_terms)
	g_debug_stream << name << " ";
	*/
	if (g_progconf.PN1) g_debug_stream << "PN1 ";
	if (g_progconf.PN2) g_debug_stream << "PN2 ";
	if (g_progconf.PN2_5) g_debug_stream << "PN2_5 ";
	if (g_progconf.PN3) g_debug_stream << "PN3 ";
	if (g_progconf.PN3_5) g_debug_stream << "PN3_5 ";
	if (g_progconf.PN_spinorbital) g_debug_stream << "spin-orbital ";
	if (g_progconf.PN_quadrupole) g_debug_stream << "quadrupole ";
	if (g_progconf.PN_spindot) g_debug_stream << "spindot ";
	g_debug_stream << "\n";

	g_debug_stream << "enabled tweaks: ";
	BOOST_FOREACH(const std::string &name, g_progconf.tweaks)
		g_debug_stream << name << " ";
	g_debug_stream << "\n";
	g_debug_stream << "Internal G = " << stringify(C_G)
		<< " c = " << stringify(C_c) << "\n";
	return 0;
}

#if 0
#include <cmath>
#include <ppl.h>
#include "cudaprop.h"
 
using namespace ::Concurrency;
int debug_cuda_main() {
	printf("Generating data...\n");
	thrust::host_vector<unsigned long> host_data(100000);
	thrust::generate(host_data.begin(), host_data.end(), rand);
	printf("generated %d numbers\n", host_data.size());

	parallel_invoke(
		[host_data]()
	{
		printf("\nRunning host code...\n");
		unsigned long host_result = thrust::reduce(host_data.cbegin(),
			host_data.cend(), 0, thrust::plus<unsigned long>());
		printf("The sum is %d\n", host_result);

		host_result = *thrust::max_element(host_data.cbegin(),
			host_data.cend(), thrust::less<unsigned long>());
		printf("The max is %d\n", host_result);
	},
		[host_data]()
	{
		printf("\nCopying data to device...\n");
		Hello hello(host_data);

		printf("\nRunning CUDA device code...\n");
		unsigned long device_result = hello.Sum();
		printf("The sum is %d\n", device_result);

		printf("\nRunning CUDA device code...\n");
		device_result = hello.Max();
		printf("The max is %d\n", device_result);
	}
	);
	return 0;
}
#endif
