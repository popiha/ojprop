#ifndef BS_HPP
#define BS_HPP

#include "system.hpp"

void bin2y(State &s, double *res);
void y2bin(double *y, State &s);

// Propagate binary one step with Bulirsch-Stoer.
// xx is time, htry initial step, eps error tolerance, hdid actual step
// hnext suggestion for next step size
void bsstep(State &s, double *xx, double htry, double eps, 
		double *hdid, double *hnext);

void bssubsteps(const State &sin, double t, double h, int nsteps, double out[]);

#endif // bs.hpp
