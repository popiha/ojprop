#ifndef GRID_HPP
#define GRID_HPP

#include "conf.hpp"
#include "disk.hpp"
#include <vector>

#include <Eigen/Dense>
using namespace Eigen;

// Functionality for a polar coordinate grid, with Miller (1971) polar
// but approximately square grid
class MillerGrid {
	protected:
	// The grid memory structure of point indexes. Stored as 2-dim array of int vectors.
	// r-indexing is first, then phi-indexing
	std::vector< std::vector< std::vector<int> > > g;

	// Indexes inside and outside the grid
	//std::vector<int> g_in;
	std::vector<int> g_out;

	// Grid parameters
	int nr, nphi;
	double rmax;

	// Precalculated values for convenience and speed
	void precalc();
	std::vector<double> cell_areas;		
	double dphi;
	double rmin, alpha;
	double total_area;
	unsigned int ncells;

	public:

	// Grid position (assume fixed orientation)
	Vector3d position;

	// Initialize grid with given number of cells and given position
	void initialize(int r_cells, int phi_cells, double rmax, Vector3d &pos);

	// Convert from cartesian coordinates to grid cell index pair. Return
	// IN_GRID if the particle is in the grid or OUT_RMIN, OUT_RMAX if it has escaped inside
	// rmin or outside rmax respectively
	bool inside(const Vector3d &v);
	bool index(const Vector3d &v, int &ind); 	
	bool index2(const Vector3d &v, int &r_index, int &phi_index); 	

	// Update grid with new positions, returns vectors of inside and outside particles
	//void update(std::vector<Vector3d>& rs, std::vector<Vector3d&> &in, std::vector<Vector3d&> &out);
	void update(std::vector<Vector3d>& rs);

	// Get contents of grid cell at index
	std::vector< std::vector<int> >& operator[](int ri) { return g[ri]; }
	unsigned int size() const { return ncells; } // Return number of grid cells

	// Get the area of the grid cell at index
	double cell_area(int r_index) const {return cell_areas[r_index]; }

	// Getters
	int get_nr() const { return nr; }
	int get_nphi() const { return nphi; }
	//std::vector<int>& inside() { return g_in; }
	std::vector<int>& outside() { return g_out; }

	// Get gridcell low and high r bounds
	double cell_rmin(int r_index) const { return rmin*exp(alpha*r_index); }
	double cell_rmax(int r_index) const { return rmin*exp(alpha*(r_index+1)); }
};


#endif // grid.hpp
