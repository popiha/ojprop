#ifndef DISK_HPP
#define DISK_HPP

#include <Eigen/Dense>
using namespace Eigen;

#include <vector>
#include <string>

struct disk {
	// Possible states
	enum ParticleState { NORMAL = 0, ACCRETED_M1, ACCRETED_M2, ESCAPED_RMIN, ESCAPED_RMAX, ESCAPED_INCLINATION };

	// Particle id
	enum ParticleInfo { DISK_M1 = 0, DISK_M2 };

	// Number of particles
	int n;

	// Particle infos, identifiers, states, positions and velocities
	std::vector<unsigned int> ids;
	std::vector<ParticleState> states;
	std::vector<ParticleInfo> infos;
	std::vector<Vector3d> rs;
	std::vector<Vector3d> vs;

	void init(int n_parts) {
		n = n_parts;
		ids.reserve(n_parts);
		states.reserve(n_parts);
		infos.reserve(n_parts);
		rs.reserve(n_parts);
		vs.reserve(n_parts);
	}
};

#endif // disk.hpp
