#ifndef SYSTEM_HPP
#define SYSTEM_HPP

#include "conf.hpp"
#include "disk.hpp"
#include "pn.hpp"

#include <Eigen/Dense>
using namespace Eigen;

#include <string>
#include <vector>


// Physical state vector plus nonphysical auxiliary variables for
// propagator use.

// Binary structure
struct binary {
	Vector3d r;	// Relative position: from m1 to m2
	Vector3d v;	// Relative velocity, derivative of r
	Vector3d rcm1, vcm1;		// In CM coordinates
	Vector3d rcm2, vcm2;		// In CM coordinates
	Vector3d s;	// Direction of m1 spin in Euclidean coordinates, unit vector
	double chi;	// Magnitude of spin

	// Constants of motion, with initial values
	double E_kep, E_kep0;	// keplerian orbital energy
	double E_gr, E_gr0;	// GR contribution (not really a constant)
	Vector3d h, h0;	// angular momentum
};

// Cellwise statistics from the grid
struct gridcell_stats {
	// z-coordinate mean, median and stddev
	double z_mean, z_median, z_dev;
};

class State {
	private:
	// TODO: Move possible initial conditions from here,
	// and eventually transfer into a model where the model is used in a separate program
	void init_binary();
	void init_binary_wo_model();
	void init_disk();

	public:
	Model m;	// Local copy of the model

	double t;	// Local (unredshifted) time

	// Binary data
	struct binary bin;	

	// Disk structure, primary & secondary
	struct disk disk1;
	struct disk disk2;

	double ttlw;	// Time transformation variable

	// Precalculated constants for convenience
	double m_tot;
	double m_rel1, m_rel2;
	double r_acc1, r_acc2;

	// Precalculated values for Post-Newtonian calculations
	pn_precalcs pc_bin, pc_disk_m1, pc_disk_m2;

	// Statistics to keep
	struct {
		// Accretion data
		int accretion_count[4];
		// Total number of accreted particles
		int accretion_total;
		// Inclination escape data
		int inclination_escape_count;
		// Total number of escaped particles
		int escape_total;

		// Statistics per gridcell, for M1 disk
		std::vector< std::vector<gridcell_stats> > cell;

		// Orbital element statistics of the grid particles:
		//		semi-major axis, eccentricity, inclination, longitude of ascending
		//		node, argument of pericenter, mean anomaly at epoch
		// for each of these, compute:
		//		mean, min, max, stddev
		double orbital_elements[6][4];
	} stats;

	//
	// Member functions

	// Construct state from given model
	void reconstruct(const Model &m);

	// Clear accumulating stat counters
	void clear_stat_accumulators() { 
		for (int i=0; i < 4; i++)
			stats.accretion_count[i] = 0;
		stats.inclination_escape_count = 0;
	}
	
	// Convert state variables to astronomical system of solar mass,
	// Astronomical Unit and Julian year
	double physical_time() const;

	// Update constants of motion
	void update_constants();

	// Update gridcellwise statistics
	void update_grid_stats();

	// Update orbital element statistics
	void update_element_stats();

	// Update CM coordinates of the binary
	void update_rcm();

	// Convert binary and disk to strings, either with internal or physical
	// representation
	std::string bin_to_string() const;
	std::string disk_to_string() const;
	std::string stat_accumulators_to_string() const;

	// Calculate disk radial statistics and return them as a string
	std::string disk_radstats_to_string();

	// Return disk orbital element statistics as a string
	std::string disk_oe_stats_to_string() const;
};


#endif // system.hpp 
