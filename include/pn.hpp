#ifndef PN_HPP
#define PN_HPP

#include <Eigen/Dense>
using namespace Eigen;

struct pn_precalcs {
	// Non-spin
	double m, gm;
	double c2, c3, c4, c5, c6, c7;
	double eta, eta2, eta3;
	double pi2;

	// Spin
	double g3, chi, chi2, q;
	double sq14eta;
	double m1;
};

/* precalculate often occurring constants in PN acceleration terms. */
void pn_do_precalcs(pn_precalcs &p, double m1, double m2, double chi, double q);

/* calculate sum of non-spin dependent PN acceleration terms of orders
 * 1, 2 & 2.5. vector sum is returned in a. */
/*
Vector3d pn_terms(double gc, double c, double m1, double m2, 
		const Vector3d &rv, const Vector3d &vv);
		*/
Vector3d pn_terms(pn_precalcs &p, const Vector3d &rv, const Vector3d &vv);

/* calculate sum of spin dependent PN acceleration terms SO and (orders
 * 1.5 and 1). spin vector in s is to be given as a cartesian 3-vector
 * sum is returned in a. */
/*
Vector3d pn_spin_terms(double gc, double c, double m1, double m2, 
		double chi, double q, 
		const Vector3d &rv, const Vector3d &vv, const Vector3d &s);
		*/
Vector3d pn_spin_terms(pn_precalcs &p, const Vector3d &rv, const Vector3d &vv, const Vector3d &s);

/* Calculate derivative of spin unit vector. */
/*
Vector3d pn_sdot(double gc, double c, double m1, double m2, 
		double chi, double q, 
		const Vector3d &rv, const Vector3d &vv, const Vector3d &s);
		*/
Vector3d pn_sdot(pn_precalcs &p, const Vector3d &rv, const Vector3d &vv, const Vector3d &s);

#endif // pn.hpp
