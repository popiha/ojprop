CC=gcc
CXX=g++
RM=rm -f
WARNS=-Wall -Wno-ignored-attributes -Wno-deprecated-declarations -Wno-misleading-indentation
#
CPPFLAGS=-std=c++11 -g -Ofast -march=native -m64 -mavx2 -flto -fopenmp -I./include -I/usr/include/eigen3 -DEIGEN_NO_DEBUG $(WARNS)
# for debug
#CPPFLAGS=-std=c++11 -g -O0 -fopenmp -I./include -I/usr/include/eigen3 $(WARNS)
LDFLAGS=$(CPPFLAGS)
LDLIBS=-lm

SRCS=src/args.cpp src/bs.cpp src/conf.cpp src/globals.cpp src/grid.cpp src/interpolation.cpp src/main.cpp src/misc.cpp src/pn.cpp src/propagator.cpp src/sdlmain.cpp src/system.cpp src/units.cpp src/viscosity.cpp
OBJS=$(subst .cpp,.o,$(SRCS))

all: ojprop

bin:
	mkdir bin

ojprop: bin $(OBJS)
	$(CXX) $(LDFLAGS) -o bin/ojprop $(OBJS) $(LDLIBS)

depend: .depend

.depend: $(SRCS)
	$(RM) ./.depend
	$(CXX) $(CPPFLAGS) -MM $^>>./.depend;

clean:
	$(RM) $(OBJS)

distclean: clean
	$(RM) *~ .depend

include .depend
